﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Com.Honeywell.Scanner.Sdk.Common.Seychellesetting;

namespace Bluetooth8060Xamarin
{
    public class DeviceSetting
    {
        public String m_text = "";
        public DeviceTextSetting.TextLineType m_TextLineType = DeviceTextSetting.TextLineType.UpLine;
        public int m_TextFontSize = TextFontSize.Medium;
        public FontSizeSetting.TextLineType m_FontSizeLineType = FontSizeSetting.TextLineType.UpLine;
        public DeviceLanguageSetting.LanguageOptions m_LanguageOption = DeviceLanguageSetting.LanguageOptions.LoEnglish;
        public DeviceTextColorSetting.TextColors m_TextColor = DeviceTextColorSetting.TextColors.Blue;
        public DeviceTextColorSetting.TextColorType m_TextColorType = DeviceTextColorSetting.TextColorType.BgColor;

        public Boolean m_EnableButtonPress = true;
        public Boolean m_bENA13_Enable = true;
        public Boolean m_bQR_Enable = true;
    }
}