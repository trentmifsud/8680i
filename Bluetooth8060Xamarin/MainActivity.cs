﻿using System;
using System.Text;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Honeywell.Scanner.Sdk.BT;
using Com.Honeywell.Scanner.Sdk.Common;
using Com.Honeywell.Scanner.Sdk.Common.Commonsetting;
using Com.Honeywell.Scanner.Sdk.Common.Seychellesetting;
using Com.Honeywell.Scanner.Sdk.Dataparser;
//using com.honeywell.scanner.sdk;


namespace Bluetooth8060Xamarin
{
    
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, IBTDeviceFoundCallBack, IConnectionCallback
    {
        public static String PERIPHERAL_ID_NAME = "com.honeywell.ScannerSample.peripheralId";
        private static readonly String TAG = "ScannerSample";
        Button buttonConnect;
        Button buttonPair;
        Button buttonDisconnect;
        Button buttonEnableScanner;
        Button buttonDisableScanner;
        TextView textViewScannedData;
        TextView textViewStatus;
        EditText editTextMacAddress;
        private BTDevice mPeripheral;
        private DeviceBTConnection seyBTConM;
        private static int ENABLE_BT_REQUEST_ID = 1;
        private static int SCAN_INTERVAL = 8000;
        private readonly int msg_start_scan = 1;
        private readonly int msg__stop_scan = 2;
        private readonly int msg_Peripheral_Found = 3;
        private readonly int msg_Peripheral_Update = 4;
        private readonly int msg_BLE_status_change = 5;
        private readonly int msg_BT_status_change = 6;



        protected override void OnResume()
        {
            base.OnResume();


            if (seyBTConM.ConnectionState == ConnectStatus.StateConnected)
            {
                //m_DeviceSetting = loadSetting();
                //configureDevice();
                ConfigureDevice();
            }

        }

    

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += FabOnClick;

            this.buttonPair = FindViewById<Button>(Resource.Id.buttonPair);
            this.buttonConnect = FindViewById<Button>(Resource.Id.buttonConnect);
            this.buttonDisconnect = FindViewById<Button>(Resource.Id.buttonDisconnect);
            this.buttonEnableScanner = FindViewById<Button>(Resource.Id.buttonEnableScanner);
            this.buttonDisableScanner = FindViewById<Button>(Resource.Id.buttonDisableScanner);
            this.textViewScannedData = FindViewById<TextView>(Resource.Id.textViewScannedData);
            this.textViewStatus = FindViewById<TextView>(Resource.Id.textViewStatus);
            this.editTextMacAddress = FindViewById<EditText>(Resource.Id.editTextMacAddress);
            this.editTextMacAddress.Text = "C0:EE:40:41:0A:1F";
            this.buttonConnect.Click += ButtonConnect_Click;
            this.buttonDisconnect.Click += ButtonDisconnect_Click;
            this.buttonEnableScanner.Click += ButtonEnableScanner_Click;
            this.buttonDisableScanner.Click += ButtonDisableScanner_Click;
            this.buttonPair.Click += ButtonPair_Click;

            ReturnCode rc = DeviceConnectionManager.CreateBTConnectionInstance(this, BTConnectionType.ClassicBT);
            if(rc != ReturnCode.Success)
            {
                //DeviceConnectionManager.CreateBTConnectionInstance(this, BTConnectionType.ClassicBT);
                AddToStatus(rc.ToString());
            }

            seyBTConM = DeviceConnectionManager.BTConnectionInstance;
            

            if (seyBTConM != null)
            {
                //seyBTConM.RemoveAllPeripherals();
                if (!seyBTConM.IsBluetoothEnabled)
                {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ActionRequestEnable);
                    StartActivityForResult(enableBtIntent, ENABLE_BT_REQUEST_ID);
                }
            }
            // displays a dialog requesting user permission to enable Bluetooth.
            //seyBTConM.RemoveAllPeripherals();
            // initialize peripherals list view
            //initializePeripheralsListView();
            //updatePeripheralsListView();
            //// initialize clearAllButton
            //updateClearAllButton();
            //// display version number
            //displayVersionNumber();
        }

        private void ButtonPair_Click(object sender, EventArgs e)
        {
            // TODO - Trent
            // var pairActivity = new Intent(this, typeof(PairActivity));
            // StartActivity(pairActivity);
            if (seyBTConM != null)
            {
                seyBTConM.StartScanDevice(10000, this);

            }

            

        }

        private void FindPeripherals()
        {
            BTDevice[] devices = seyBTConM.GetPeripherals();
            StringBuilder sb = new StringBuilder();

            foreach (BTDevice bTDevice in devices)
            {
                sb.Append(bTDevice.Address);
                sb.Append(" ");
            }

            AddToStatus("FOUND : " + sb.ToString());

            if (devices.Length > 0)
            {
                this.editTextMacAddress.Text = devices[0].Address;
            }
        }

        private void ButtonDisableScanner_Click(object sender, EventArgs e)
        {

                }

        private void ButtonEnableScanner_Click(object sender, EventArgs e)
        {

           
        }

        private void ButtonDisconnect_Click(object sender, EventArgs e)
        {
            try
            {

                seyBTConM.DisconnectDevice();
                AddToStatus("Disconnected");
            }
            catch (Exception ex)
            {

            }
        }

        private void ButtonConnect_Click(object sender, EventArgs e)
        {
            try
            {
                //ReturnCode rc = seyBTConM.ConnectDevice(new BTDevice(this.editTextMacAddress.Text, "8680"));
                //if(rc != ReturnCode.Success)
                //{
                //    AddToStatus(problem);
                //}

                //seyBTConM.SetConnectionListener(this);
                //ReturnCode rc = seyBTConM.ConnectDevice(mPeripheral);
                //if (rc != ReturnCode.Success)
                //{
                //    String problem = "NOT Connected : " + rc.ToString();
                //    AddToStatus(problem);
                //}
                //else
                //{
                //    ConfigureDevice();
                //}
                
            }
            catch (Exception ex)
            {
                Log.Error(TAG, "! Connect peripheral failed, " + ex.ToString());
            }

        }


        private void AddToStatus(string status)
        {
            RunOnUiThread(() => {
                this.textViewStatus.Text = string.Empty;
                this.textViewStatus.Text = status;
            });
        }


        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }

        public void OnPeripheralFound(BTDevice p0)
        {
            Log.Debug(TAG, "onPeripheralDiscovered " + p0.ToString());
            AddToStatus("Found : " + p0.Address);
            this.editTextMacAddress.Text = p0.Address;


            //BTDevice[] devices = seyBTConM.GetPeripherals();
            //StringBuilder sb = new StringBuilder();

            //foreach (BTDevice bTDevice in devices)
            //{
            //    sb.Append(bTDevice.Address);
            //    sb.Append(" ");
            //}

            //AddToStatus("FOUND : " + sb.ToString());

            //if (devices.Length > 0)
            //{
            //    this.editTextMacAddress.Text = devices[0].Address;
            //}
        }

        public void OnPeripheralUpdate(BTDevice p0)
        {
            Log.Debug(TAG, "OnPeripheralUpdate " + p0.ToString());
            AddToStatus("PeripheralUpdate : " + p0.Address);
        }

        public void OnScanStatus(int status)
        {
            if (status == 0)// start scan
            {

                AddToStatus("Scan Status : start");
            }
            else if (status == 1)// stop scan
            {
                AddToStatus("Scan Status : stop");
                FindPeripherals();
            }

        }

        public void OnButtonPressedReceived(BtnNotifyData btnData)
        {
            try
            {
                String str = "";
                if (btnData.MBtnNofication == BtnNotifyData.BtnLeftbuttonPressed)
                {
                    str = "Left button pressed!" + "\r\n";
                }
                else if (btnData.MBtnNofication == BtnNotifyData.BtnRightbuttonPressed)
                {
                    str = "Right button pressed!" + "\r\n";
                }
                else if (btnData.MBtnNofication == BtnNotifyData.BtnBothbuttonPressed)
                {
                    str = "Both button pressed!" + "\r\n";
                }
                AddToStatus(str);

            }
            catch (Exception ex) { }
        }

        public void OnBarcodeDataReceived(BarcodeData barcodeData)
        {

            try
            {
                
                String strBarcode = System.Text.Encoding.Default.GetString(barcodeData.GetByteData());
                AddToStatus("SCANNED : " + strBarcode);
            }
            catch (Exception ex) { }
        }
        private Android.Support.V7.App.AlertDialog mConnectingDialog;

        public void OnConnectionStatus(int status, string p1)
        {
            if (status == ConnectStatus.StateConnected)
            {
                Log.Debug(TAG, "onConnected");
                
                ConfigureDevice();
            }
            else if (status == ConnectStatus.StateDisconnected)
            {
                Log.Debug(TAG, "onDisconnected" );

                if ((mConnectingDialog != null) && mConnectingDialog.IsShowing) { mConnectingDialog.Dismiss(); };
                seyBTConM.SetConnectionListener(null);
                AddToStatus("Disconnected");
                
            }
        }
    

        public void OnDataTransmitted(int p0, int p1)
        {
            //nope
        }

        public void OnGetScannerInfo(DeviceInfo p0)
        {
            Log.Debug(TAG, "onGetScannerInfo " + DeviceInfo.MStrName);
            String str_serial_num = "Serial Number: " + DeviceInfo.MStrSerialNum + "\r\n";
            String str_app_version = "App Version: " + DeviceInfo.MStrAppVersion + "\r\n";
            String str_app_data = "App Data: " + DeviceInfo.MStrAppDate + "\r\n";
            String str_boot_version = "Boot Version: " + DeviceInfo.MStrBootVersion + "\r\n";
            String str_bsp_version = "BSP Version: " + DeviceInfo.MStrBSPVersion + "\r\n";
            String str = str_serial_num + str_app_version + str_app_data + str_boot_version + str_bsp_version;
            AddToStatus(str);
        }

        public void OnMenuCommandReceived(MenuData p0)
        {
            //nope
        }

        private DeviceSetting m_DeviceSetting = new DeviceSetting();

        private void ConfigureDevice()
        {
            DeviceLanguageSetting language = new DeviceLanguageSetting(m_DeviceSetting.m_LanguageOption);
            seyBTConM.SetConfiguration(language);

            DeviceTextSetting text = new DeviceTextSetting(m_DeviceSetting.m_text, m_DeviceSetting.m_TextLineType);
            seyBTConM.SetConfiguration(text);


            FontSizeSetting fontSetting = new FontSizeSetting(m_DeviceSetting.m_TextFontSize, m_DeviceSetting.m_FontSizeLineType);
            seyBTConM.SetConfiguration(fontSetting);

            DeviceTextColorSetting text_setting = new DeviceTextColorSetting();

            text_setting.SetTextColor(m_DeviceSetting.m_TextColorType, m_DeviceSetting.m_TextColor);
            seyBTConM.SetConfiguration(text_setting);

            String command = "";
            if (m_DeviceSetting.m_bENA13_Enable)
            {
                command = "E13ENA1";
            }
            else
            {
                command = "E13ENA0";
            }

            if (m_DeviceSetting.m_bQR_Enable)
            {
                command += ";";
                command += "QRCENA1";
            }
            else
            {
                command += ";";
                command += "QRCENA0";
            }
            DeviceConfiguration barcode_Configuration = new DeviceConfiguration(command);
            seyBTConM.SetConfiguration(barcode_Configuration);

            DeviceButtonSetting bc = new DeviceButtonSetting(m_DeviceSetting.m_EnableButtonPress);
            seyBTConM.SetConfiguration(bc);

            DeviceConfiguration con = new DeviceConfiguration("E13ENA1");

            seyBTConM.SetConfiguration(con);
        }
    }
}

