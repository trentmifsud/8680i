﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Java.Util;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Honeywell.ExternalScanner.ScannerCommands
{
    public class Commands
    {
        #region scanner command strings
        // SYN M CR
        public static readonly byte[] SerialCommandPrefix = new byte[] { 22, 77, 13 };
        public static readonly string DeactivateTrigger = "TRGITG1!";
        public static readonly string ActivateTrigger = "TRGITG0!";
        public static readonly string DefaultScanner = "DEFALT!";
        public static readonly string ScannerBeepOne = "BEPEXE1!";
        public static readonly string IconBarOn = "GUIICN1!";
        public static readonly string IconBarOff = "GUIICN0!";
        public static readonly string UnpairWhenCharging = "BT_DSC2!";
        public static readonly string ShowSoftwareRevision = "REVINF!";
        public static readonly string BattInfo = "BTRINF!";
        public static readonly string HardwareButtonsOn = "GUIBNE1.";
        public static readonly string HardwareButtonsOff = "GUIBNE0.";
        public static readonly string UpdateMenu = "GUITXT00";

        #endregion

        #region serial port guid
        public static readonly UUID uuidSerialPort = UUID.FromString("00001101-0000-1000-8000-00805f9b34fb");
        #endregion
    }
}