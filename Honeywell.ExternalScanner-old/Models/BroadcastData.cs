﻿using System;

namespace Honeywell.ExternalScanner.Models
{
    public class BroadcastData : Models.BaseData
    {
        public BroadcastData(String Message)
        {
            this.Message = Message;
        }
    }
}

