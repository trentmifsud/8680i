﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Honeywell.ExternalScanner.BroadcastReceivers;

namespace Honeywell.ExternalScanner.Models
{
    public class BluetoothData : BaseData
    {
        Android.Bluetooth.BluetoothDevice bluetoothDevice;
        BluetoothReceiver.BluetoothState bluetoothState;

        public BluetoothData()
        {

        }

        public BluetoothData(string message, BluetoothReceiver.BluetoothState bluetoothState) : base(message)
        {
            this.bluetoothState = bluetoothState;
        }

        public BluetoothData(BluetoothDevice bluetoothDevice, BluetoothReceiver.BluetoothState bluetoothState)
        {
            this.BluetoothDevice = bluetoothDevice ?? throw new ArgumentNullException(nameof(bluetoothDevice));
            this.bluetoothState = bluetoothState;
        }

        public BluetoothData(BluetoothDevice bluetoothDevice, string message, BluetoothReceiver.BluetoothState bluetoothState)
        {
            this.BluetoothDevice = bluetoothDevice ?? throw new ArgumentNullException(nameof(bluetoothDevice));
            this.Message = message;
            this.bluetoothState = bluetoothState;

        }

        public BluetoothDevice BluetoothDevice { get => bluetoothDevice; set => bluetoothDevice = value; }
        public BluetoothReceiver.BluetoothState BluetoothState { get => bluetoothState; set => bluetoothState = value; }
    }
}