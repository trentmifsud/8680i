﻿using Android.Bluetooth;
using Java.Lang.Reflect;
using System;
using System.Collections.Generic;

namespace Honeywell.ExternalScanner
{
    public abstract class AbstractScanner
    {
        #region fields
        public static readonly string TAG = "Honeywell.ExternalScanner";
        #endregion

        #region abstract methods
        public abstract void Initialise();
        public abstract dynamic GetStatus();
        public abstract dynamic IsConnected();
        public abstract dynamic Connect();
        public abstract dynamic SendConfigurationToScanner(String configString);
        public abstract dynamic SendConfigurationToScanner(params String [] configStrings);
        public abstract dynamic Disconnect();
        public abstract dynamic RemoveOtherBluetoothLikeDevices();
        #endregion


        #region non-abstract methods
        public ICollection<BluetoothDevice> GetBondedDevices()
        {
            try
            {
                return BluetoothAdapter.DefaultAdapter.BondedDevices;
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Cannot get bonded devices : " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Gets the Mac address of the Android Host Device. 
        /// </summary>
        /// <returns></returns>
        public static string GetHostsBluetoothMacAddress()
        {
            Java.Lang.String bluetoothMacAddress;
            try
            {
                Java.Lang.Class c = Java.Lang.Class.ForName("android.os.SystemProperties");
                Method get = c.GetMethod("get", Java.Lang.Class.FromType(typeof(Java.Lang.String)));
                bluetoothMacAddress = (Java.Lang.String)(get.Invoke(c, "ro.hsm.bt.addr"));
            }
            catch (global::Java.Lang.Exception ex)
            {
                //MainApp.Logger.Error(ex, "Exception getting mac address");

                bluetoothMacAddress = new Java.Lang.String("unknown");
            }
            return bluetoothMacAddress.ToString();
        }
        #endregion
    }
}
