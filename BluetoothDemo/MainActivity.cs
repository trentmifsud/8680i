﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Android.Bluetooth;
using System.Text;
using System.Collections.Generic;
using Java.Util;
using Java.IO;
using Android.Content;
using Android;
using Android.Content.PM;
using System.Threading.Tasks;
using BluetoothDemo.Models;
using BluetoothDemo.BroadcastReceivers;
using Android.Nfc;

namespace BluetoothDemo
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        readonly string[] PermissionsRequired =
          {
              Manifest.Permission.AccessCoarseLocation,
              Manifest.Permission.Bluetooth,
              Manifest.Permission.BluetoothAdmin,
            };

        const int RequestLocationId = 0;
        private const string Scanner8680 = "8680i";
        BluetoothHelper bluetoothHelper = new BluetoothHelper();
        Button buttonConnect;
        Button buttonPair;
        Button buttonDisconnect;
        Button buttonEnableScanner;
        Button buttonDefaultScanner;
        Button buttonDisableScanner;
        Button buttonIconMenuOff;
        Button buttonIconMenuOn;
        Button buttonHwbuttonsOn;
        Button buttonHwbuttonsOff;
        Button buttonBeep;
        Button buttonUnpairWhenCharging;
        Button buttonBatInfo;
        Button buttonPair2D;
        Button buttonUnPair2D;
        TextView textViewScannedData;
        Button buttonShowSoftwareRevision;
        TextView textViewStatus;
        EditText editTextMacAddress;
        EditText editTextUpdateDisplay;
        Button buttonUpdateDisplayText;

        BluetoothReceiver bluetoothReceiver = new BluetoothReceiver();
        public MainActivity()
        {   

        }

        _8680Model current860Info = null;

        // This method will be invoked whenever a SyncData message is posted
        [Muni.Subscribe]
        public void OnSyncComplete(BaseData data)
        {
            if (data is _8680BroadcastData)
            {
                current860Info = new _8680Model(data.Message);
                Set8060Info(current860Info);
            }
            else if (data is BroadcastData)
            {
                AddToStatus(data.Message);
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            MainApp.Bus.Register(this);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            this.buttonPair = FindViewById<Button>(Resource.Id.buttonPair);
            this.buttonConnect = FindViewById<Button>(Resource.Id.buttonConnect);
            this.buttonDisconnect = FindViewById<Button>(Resource.Id.buttonDisconnect);
            this.buttonEnableScanner = FindViewById<Button>(Resource.Id.buttonEnableScanner);
            this.buttonDisableScanner = FindViewById<Button>(Resource.Id.buttonDisableScanner);
            this.textViewScannedData = FindViewById<TextView>(Resource.Id.textViewScannedData);
            this.textViewStatus = FindViewById<TextView>(Resource.Id.textViewStatus);
            this.editTextMacAddress = FindViewById<EditText>(Resource.Id.editTextMacAddress);
            this.buttonDefaultScanner = FindViewById<Button>(Resource.Id.buttonDefaultScanner);
            this.editTextUpdateDisplay = FindViewById<EditText>(Resource.Id.editTextUpdateDisplay);
            this.buttonUpdateDisplayText = FindViewById<Button>(Resource.Id.buttonUpdateDisplayText);
            this.buttonHwbuttonsOn = FindViewById<Button>(Resource.Id.buttonHwbuttonsOn);
            this.buttonHwbuttonsOff = FindViewById<Button>(Resource.Id.buttonHwbuttonsOff);
            this.buttonIconMenuOff = FindViewById<Button>(Resource.Id.buttonIconMenuOff);
            this.buttonIconMenuOn = FindViewById<Button>(Resource.Id.buttonIconMenuOn);
            this.buttonUnpairWhenCharging = FindViewById<Button>(Resource.Id.buttonUnpairWhenCharging);
            this.buttonShowSoftwareRevision = FindViewById<Button>(Resource.Id.buttonShowSoftwareRevision);
            this.buttonBatInfo = FindViewById<Button>(Resource.Id.buttonBatInfo);
            this.buttonPair2D = FindViewById<Button>(Resource.Id.buttonPair2D);
            this.buttonPair2D.Click += ButtonPair2D_Click;
            this.buttonUnPair2D = FindViewById<Button>(Resource.Id.buttonUnPair2D);
            this.buttonUnPair2D.Click += ButtonUnPair2D_Click;
            this.buttonBeep = FindViewById<Button>(Resource.Id.buttonBeep);
            
            this.buttonIconMenuOff.Click += ButtonIconMenuOff_Click;
            this.buttonIconMenuOn.Click += ButtonIconMenuOn_Click;
            this.buttonHwbuttonsOn.Click += ButtonHwbuttonsOn_Click;
            this.buttonHwbuttonsOff.Click += ButtonHwbuttonsOff_Click;
            this.buttonUnpairWhenCharging.Click += ButtonUnpairWhenCharging_Click;
            this.buttonBeep.Click += ButtonBeep_Click;

            this.buttonConnect.Click += ButtonConnect_Click;
            this.buttonDisconnect.Click += ButtonDisconnect_Click;
            this.buttonEnableScanner.Click += ButtonEnableScanner_Click;
            this.buttonDisableScanner.Click += ButtonDisableScanner_Click;
            this.buttonPair.Click += ButtonPair_Click;
            this.buttonDefaultScanner.Click += ButtonDefaultScanner_Click;
            this.buttonUpdateDisplayText.Click += ButtonUpdateDisplayText_Click;
            this.buttonBatInfo.Click += ButtonBatInfo_Click;
            this.buttonShowSoftwareRevision.Click += ButtonShowSoftwareRevision_Click;

            IntentFilter filter = new IntentFilter();
            filter.AddAction(BluetoothDevice.ActionAclConnected);
            filter.AddAction(BluetoothDevice.ActionAclDisconnected);
            filter.AddAction(BluetoothDevice.ActionAclDisconnectRequested);
            filter.AddAction(BluetoothDevice.ActionBondStateChanged);
            this.RegisterReceiver(bluetoothReceiver, filter);
            

        }

        private void ButtonUnPair2D_Click(object sender, EventArgs e)
        {
            var unPairActivity2D = new Intent(this, typeof(AztecPair));
            unPairActivity2D.PutExtra("Mode", "UnPair");
            StartActivity(unPairActivity2D);
        }

        private void ButtonPair2D_Click(object sender, EventArgs e)
        {
            var pairActivity2D = new Intent(this, typeof(AztecPair));
            pairActivity2D.PutExtra("Mode", "Pair");
            StartActivity(pairActivity2D);
        }

        private void ButtonBatInfo_Click(object sender, EventArgs e)
        {
            bluetoothHelper.SendToScanner(bluetoothHelper.BattInfo, bluetoothHelper.SerialCommandPrefix);
        }

        private void ButtonShowSoftwareRevision_Click(object sender, EventArgs e)
        {
            bluetoothHelper.SendToScanner(bluetoothHelper.ShowSoftwareRevision, bluetoothHelper.SerialCommandPrefix);
        }

        private void ButtonUnpairWhenCharging_Click(object sender, EventArgs e)
        {
            bluetoothHelper.SendToScanner(bluetoothHelper.UnpairWhenCharging, bluetoothHelper.SerialCommandPrefix);
        }

        private void ButtonIconMenuOn_Click(object sender, EventArgs e)
        {
            bluetoothHelper.SendToScanner(bluetoothHelper.IconBarOn, bluetoothHelper.SerialCommandPrefix);
        }

        private void ButtonIconMenuOff_Click(object sender, EventArgs e)
        {
            bluetoothHelper.SendToScanner(bluetoothHelper.IconBarOff, bluetoothHelper.SerialCommandPrefix);
        }


        

        private void ButtonPairedDevices_Click(object sender, EventArgs e)
        {
            bluetoothHelper.PairedDevices();

        }

        private void ButtonBeep_Click(object sender, EventArgs e)
        {
            AddToStatus("Beep");
            bluetoothHelper.SendToScanner(bluetoothHelper.ScannerBeepOne, bluetoothHelper.SerialCommandPrefix);
        }

        private void ButtonHwbuttonsOff_Click(object sender, EventArgs e)
        {
            AddToStatus("hardware buttons off");
            bluetoothHelper.SendToScanner(bluetoothHelper.HardwareButtonsOff, bluetoothHelper.SerialCommandPrefix);
            
        }

        private void ButtonHwbuttonsOn_Click(object sender, EventArgs e)
        {
            AddToStatus("hardware buttons on");
            bluetoothHelper.SendToScanner(bluetoothHelper.HardwareButtonsOn, bluetoothHelper.SerialCommandPrefix);
        }

        private void ButtonSerialPort_Click(object sender, EventArgs e)
        {
            var pairActivity = new Intent(this, typeof(BarcodeActivity));
            pairActivity.PutExtra("Type", "SerialPort");
            StartActivity(pairActivity);
        }
     

        private void ButtonUpdateDisplayText_Click(object sender, EventArgs e)
        {
            // GUITXT00{0}
         
            bluetoothHelper.UpdateDisplay(this.editTextUpdateDisplay.Text, bluetoothHelper.SerialCommandPrefix);
        }

        private void ButtonDefaultScanner_Click(object sender, EventArgs e)
        {
            bluetoothHelper.SendToScanner(bluetoothHelper.DefaultScanner, bluetoothHelper.SerialCommandPrefix);

        }

        private void ButtonPair_Click(object sender, EventArgs e)
        {
            var pairActivity = new Intent(this, typeof(BarcodeActivity));
            pairActivity.PutExtra("Type", "Pair");
            StartActivity(pairActivity);
        }

        private void ButtonDisableScanner_Click(object sender, EventArgs e)
        {
            bluetoothHelper.SendToScanner(bluetoothHelper.DeactivateTrigger, bluetoothHelper.SerialCommandPrefix);
        }

        private void ButtonEnableScanner_Click(object sender, EventArgs e)
        {
            bluetoothHelper.SendToScanner(bluetoothHelper.ActivateTrigger, bluetoothHelper.SerialCommandPrefix);
        }

        private void ButtonDisconnect_Click(object sender, EventArgs e)
        {
            bluetoothHelper.Cancel();
        }

        private void ButtonConnect_Click(object sender, EventArgs e)
        {
            bluetoothHelper.Start(Scanner8680);
        }


        private void AddToStatus(string status)
        {
            RunOnUiThread(() => {
                this.textViewStatus.Text = string.Empty;
                this.textViewStatus.Text = status;
             });
        }

        private void Set8060Info(_8680Model model)
        {
            RunOnUiThread(() => {
                this.textViewStatus.Text = string.Empty;
                this.textViewScannedData.Text = model.ToString();
            });
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

       
        protected override void OnDestroy()
        {
            base.OnDestroy();
            this.UnregisterReceiver(this.bluetoothReceiver);
            bluetoothHelper.Dispose();
        }
    }
}

