﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BluetoothDemo.Logging;
using Muni;

namespace BluetoothDemo
{
    [Application]
    public class MainApp : Application
    {
        private static Muni.Bus bus;
        public static MainApp Instance;

        public MainApp(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {

        }

        public static Muni.Bus Bus { get => bus; }

        public override void OnCreate()
        {
            base.OnCreate();
            bus = new Bus(ThreadEnforcer.AnyThread);
            Instance = this;

        }

        private static ILoggingService _logger;
        public static ILoggingService Logger
        {
            get
            {
                if (_logger == null) _logger = CrossLoggingService.Current;
                return _logger;
            }
        }

    }
}