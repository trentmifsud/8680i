﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ZXing;
using ZXing.Common;
using ZXing.OneD;

namespace BluetoothDemo
{
    [Activity(Label = "PairActivity")]
    public class BarcodeActivity : Activity
    {
        ImageView imageBarcode;
        TextView imageBarcodeText;
        ZXing.BarcodeWriter barcodeWriter;
        ZXing.Aztec.AztecWriter aztecWriter;


        //  Option 'Leading FNC3 - Metrologic' configuration symbol.
        //  The label starts with the special symbol [FNC3], followed by the string 'LnkB' 
        //  and then the Bluetooth address (12 hex digits, no colons). For example: LnkB0400fd002031. 

        // "\u00f3" + "LnkB" + "123456789011"

        private Writer writer;
        string fnc_encoded = "10111100010";
        /// <summary>
        /// This is TODO and not working as yet.
        /// </summary>
        /// <param name="bundle"></param>
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.pair_activity);
            imageBarcode = FindViewById<ImageView>(Resource.Id.imageBarcode);
            imageBarcodeText = FindViewById<TextView>(Resource.Id.imageBarcodeText);

            String typeOfbarcodeToShow = Intent.Extras.GetString("Type");
            if (typeOfbarcodeToShow.Equals("Pair"))
            {
                ShowPairBarcode();
            }
            else if(typeOfbarcodeToShow.Equals("SerialPort"))
            {
                ShowSppBarcode();
            }
        }

        private void ShowSppBarcode()
        {   
            aztecWriter = new ZXing.Aztec.AztecWriter();
            string command = String.Format("PAPSPP;BT_DNG1.");
            imageBarcodeText.Visibility = ViewStates.Invisible;
            imageBarcode.SetImageResource(Resource.Drawable.papspp_bt_dng1);

        }
        

        private void ShowPairBarcode()
        {
            writer = new Code128Writer();
            aztecWriter = new ZXing.Aztec.AztecWriter();
            string command = String.Format("PAPSPP;BT_DNG1;BT_ADR{0}.", BluetoothHelper.GetBluetoothMacAddress());
            BitMatrix bitMatrix = writer.encode("\u00f3" + "LnkB" + BluetoothHelper.GetBluetoothMacAddress(),BarcodeFormat.CODE_128, 1600, 800);

            String actual = matrixToString(bitMatrix);


            imageBarcodeText.Text = "[FNC3]LnkB" + BluetoothHelper.GetBluetoothMacAddress();
            //imageBarcode.Rotation = 90f;

            barcodeWriter = new ZXing.BarcodeWriter
            {
                Format = ZXing.BarcodeFormat.CODE_128,
                Options = new EncodingOptions
                {
                    PureBarcode = false
                }
            };

            //string encode_this = fnc_encoded + "LnkB" + BluetoothHelper.GetBluetoothMacAddress().ToUpper();
            var barcode = barcodeWriter.Write(bitMatrix);

            imageBarcode.SetImageBitmap(barcode);
        }

        public static String matrixToString(BitMatrix result)
        {
           
            StringBuilder builder = new StringBuilder(result.Width);
            for (int i = 0; i < result.Width; i++)
            {

                builder.Append(result[i, 0] ? '1' : '0');

            }
            return builder.ToString();
        }

        private byte[] buildMenuCommand(String command)
        {
           
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("buildMenuCommand, command: ");
            stringBuilder.Append(command);

            int prefixSize = 3;

            //byte[] SerialCommandPrefix = new byte[] { 22, 77, 13 };

            try
            {   
                byte[] commandBytes = Encoding.ASCII.GetBytes(command);
                byte[] ret = new byte[(commandBytes.Length + 3)];
                int i = 0;
                ret[0] = (byte)22;
                ret[1] = (byte)77;
                ret[2] = (byte)13;
                while (true)
                {
                    int i2 = i;
                    if (i2 >= commandBytes.Length)
                    {
                        return ret;
                    }
                    ret[prefixSize + i2] = commandBytes[i2];
                    i = i2 + 1;
                }
            }
            catch (Exception e)
            {
                MainApp.Logger.Error(e, "");
                return null;
            }
        }
    }
}