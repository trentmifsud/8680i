﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BluetoothDemo.Models
{
    public abstract class BaseData
    {
        protected BaseData()
        {
        }

        protected BaseData(string message)
        {
            Message = message;
        }

        public String Message { get; set; }

    }
}