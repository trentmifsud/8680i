﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BluetoothDemo.Models
{
    class _8680Model
    {
        private string productName;
        private string bootRev;
        private string bootsoftwarePartNo;
        private string softwarePartNo;
        private string softwareRev;
        private string serialNo;
        private string supportedIf;

        public _8680Model()
        {

        }

        public _8680Model(string infoString)
        {
            ParseMembers(infoString);
        }

        public string ProductName { get => productName; set => productName = value; }
        public string BootRev { get => bootRev; set => bootRev = value; }
        public string BootsoftwarePartNo { get => bootsoftwarePartNo; set => bootsoftwarePartNo = value; }
        public string SoftwarePartNo { get => softwarePartNo; set => softwarePartNo = value; }
        public string SoftwareRev { get => softwareRev; set => softwareRev = value; }
        public string SerialNo { get => serialNo; set => serialNo = value; }
        public string SupportedIf { get => supportedIf; set => supportedIf = value; }

        private void ParseMembers(string infoString)
        {
            string[] lines = infoString.Split('\r','\n');
            foreach(string line in lines)
            {
                if(line.Contains("Product Name"))
                {
                    this.productName = GetSubString(line);
                }
                else if (line.Contains("Boot Revision"))
                {
                    this.bootRev = GetSubString(line);
                }
                else if (line.Contains("Boot Software Part Number"))
                {
                    this.bootsoftwarePartNo = GetSubString(line);
                }
                else if (line.Contains("Software Part Number"))
                {
                    this.softwarePartNo = GetSubString(line);
                }
                else if (line.Contains("Software Revision"))
                {
                    this.softwareRev = GetSubString(line);
                }
                else if (line.Contains("Serial Number"))
                {
                    this.serialNo = GetSubString(line);
                }
                else if (line.Contains("Supported IF"))
                {
                    this.supportedIf = GetSubString(line);
                }
            }
        }

        private string GetSubString(string line)
        {
            return line.Substring(line.IndexOf(":")).Trim();
        }

        public override string ToString()
        {
            return string.Format("Name : 8680. Serial : {0}. Rev : {1}", this.serialNo, this.softwareRev);
        }
    }
}