﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BluetoothDemo.Helpers
{
    class Images
    {
        public static bool SaveBitmap(Bitmap image, string imageFileName)
        {
            try
            {
                var filePath = System.IO.Path.Combine(GetSaveDir(), imageFileName);
                var stream = new System.IO.FileStream(filePath, System.IO.FileMode.Create);
                image.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                stream.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool ImageExists(string imageFileName)
        {
            try
            {
                var filePath = System.IO.Path.Combine(GetSaveDir(), imageFileName);
                return File.Exists(filePath);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SaveBitmap(byte[] imageBytes, string imageFileName)
        {
            try
            {

                var filePath = System.IO.Path.Combine(GetSaveDir(), imageFileName);
                var stream = new System.IO.FileStream(filePath, System.IO.FileMode.Create);
                Bitmap bm = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                bm.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                stream.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Bitmap GetBitmap(string imageFileName)
        {
            try
            {
                var sdCardPath = GetSaveDir();
                var filePath = System.IO.Path.Combine(sdCardPath, imageFileName);
                return BitmapFactory.DecodeFile(filePath);
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        private static string GetSaveDir()
        {
            // Get the directory for the user's public pictures directory.
            return Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryPictures).AbsolutePath;
            // System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        }
    }
}