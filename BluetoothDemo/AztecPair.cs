﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Util;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Java.Lang;

namespace BluetoothDemo
{
    [Activity(Label = "AztecPair")]
    public class AztecPair : Activity
    {

        // NOTE YOU CAN CHANGE THIS TO SEND COMMANDS TO THE SCANNER ON PAIR
        public const string EXTRA_PROVISIONING_COMMANDS = "PAPSPP;BT_DNG4;";

        #region variables
        static readonly string[] PERMISSIONS_TO_REQUEST = { Manifest.Permission.WriteExternalStorage };
        const int RC_WRITE_EXTERNAL_STORAGE_PERMISSION = 1000;
        private WebView mWebView;
        private ImageView imageView1;
        private Context mContext;
        public Handler mMainHandler;
        private string[] resultHtml = new string[1];
        private  ConditionVariable resultCond = new ConditionVariable();
        private string mMode = "";
        private static string TAG = "AztecPair";
        public const string BLANK_URL = "about:blank";
        public const string ACTION_CONNECT_SPP = "ACTION_CONNECT_SPP";
        public const string ACTION_DISCONNECT = "ACTION_DISCONNECT";
        private WebViewClientForAztecPairing webViewClientForAztecPairing;
        private const string imageFileNamePair = "AztecPair.png";
        private const string imageFileNameUnPair = "AztecUnPair.png";
        #endregion

        /// <summary>
        /// Injects the supplied Java object into the WebView.
        /// </summary>
        private class JsObject : Java.Lang.Object
        {
            AztecPair aztecPair;
            public JsObject(AztecPair aztecPair)
            {
                this.aztecPair = aztecPair;
            }

            [JavascriptInterface]
            public void getHtml(string text)
            {
                aztecPair.RunOnUiThread(() =>
                {
                    Log.Debug(TAG, "getHtml");
                    aztecPair.resultHtml[0] = text;
                    aztecPair.resultCond.Open();
                });
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            mContext = ApplicationContext.ApplicationContext;
            mMainHandler = new Handler(mContext.MainLooper);

            string typeOfbarcodeToShow = Intent.Extras.GetString("Mode");
            string cachedFileName = string.Empty;
            if (typeOfbarcodeToShow.Equals("Pair"))
            {
                mMode = ACTION_CONNECT_SPP;
                cachedFileName = imageFileNamePair;
            }
            else if (typeOfbarcodeToShow.Equals("UnPair"))
            {
                mMode = ACTION_DISCONNECT;
                cachedFileName = imageFileNameUnPair;
            }

            if (!Helpers.Images.ImageExists(cachedFileName))
            {

                bool okToSave = RequestExternalStoragePermissionIfNecessary(RC_WRITE_EXTERNAL_STORAGE_PERMISSION);

                SetContentView(Resource.Layout.aztec_pair);
                mWebView = FindViewById<WebView>(Resource.Id.webView1);
                mWebView.Settings.CacheMode = CacheModes.NoCache;
                mWebView.Settings.JavaScriptEnabled = true;
                JsObject js = new JsObject(this);

                mWebView.AddJavascriptInterface(js, "injectedJsObject");
                RenderHtml(mMode);
            }
            else
            {
                SetContentView(Resource.Layout.aztec_pair_with_image);
                imageView1 = FindViewById<ImageView>(Resource.Id.imageView1);
                imageView1.SetImageBitmap(Helpers.Images.GetBitmap(cachedFileName));
            }
        }

        public string RenderHtml(string mode)
        {
            render(mode);
            if (resultCond.Block(5000))
            {
                Log.Debug(TAG, "renderHtml:\n" + resultHtml[0]);
            }
            else
            {
                Log.Debug(TAG, "renderHtml Failed");
            }
            return resultHtml[0];
        }

        public class WebViewClientForAztecPairing : WebViewClient
        {
            AztecPair aztecPair = null;
            string renderMode = "";
            public WebViewClientForAztecPairing(AztecPair aztecPair, string renderMode)
            {
                this.aztecPair = aztecPair;
                this.renderMode = renderMode;
            }

            public override bool ShouldOverrideUrlLoading(WebView view, IWebResourceRequest request)
            {
                view.LoadUrl(request.Url.ToString());
                return true;
            }

            public override void OnPageFinished(WebView view, string url)
            {
                base.OnPageFinished(view, url);
                if(AztecPair.BLANK_URL.Equals(url))
                {
                    return;
                }
                string str = "";

                switch (renderMode)
                {
                    
                    case AztecPair.ACTION_CONNECT_SPP:
                        {
                            Log.Debug("TAG", "ACTION_CONNECT_SPP");
                            
                            string cmd = string.Format(AztecPair.EXTRA_PROVISIONING_COMMANDS +  "BT_ADR{0}.", BluetoothHelper.GetBluetoothMacAddress());
                            str = "javascript:HsmCreateAztec('" + cmd + "')";
                            view.LoadUrl(str);
                            if (!Helpers.Images.ImageExists(imageFileNamePair))
                            {
                                // give time fot the control to draw
                                new Handler().PostDelayed(delegate
                                {
                                    byte[] bitmapBytes = Helpers.ScreenShot.TakeScreenCapture(view);
                                    Helpers.Images.SaveBitmap(bitmapBytes, imageFileNamePair);
                                    // Your code here
                                }, 2000);
                                
                            }
                            break;
                        }
                    case AztecPair.ACTION_DISCONNECT:
                        {
                            Log.Debug("TAG", "ACTION_DISCONNECT");
                            string cmd = string.Format("BT_RMV.");
                            str = "javascript:HsmCreateAztecOne('" + cmd + "','" + "Scan to Disconnect" + "')";
                            view.LoadUrl(str);

                            if (!Helpers.Images.ImageExists(imageFileNameUnPair))
                            {
                                // give time fot the control to draw
                                new Handler().PostDelayed(delegate
                                {
                                    byte[] bitmapBytes = Helpers.ScreenShot.TakeScreenCapture(view);
                                    Helpers.Images.SaveBitmap(bitmapBytes, imageFileNameUnPair);
                                    // Your code here
                                }, 2000);

                            }

                            break;
                        }
                    default:
                        {
                            Log.Warn("TAG", "Invalid mode: " + renderMode);
                            break;
                        }
                }

                view.LoadUrl("javascript:window.injectedJsObject.getHtml" +
                        "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                
                
               

            }
        }

        public void render(string mode)
        {
            mMode = mode;
            resultHtml[0] = "";
            resultCond.Close();
            webViewClientForAztecPairing = new WebViewClientForAztecPairing(this, mode);
            mWebView.SetWebViewClient(webViewClientForAztecPairing);
            mWebView.LoadUrl("file:///android_asset/barcode.html");
            

           
        }

        bool RequestExternalStoragePermissionIfNecessary(int requestCode)
        {
            if (Android.OS.Environment.MediaMounted.Equals(Android.OS.Environment.ExternalStorageState))
            {
                if (CheckSelfPermission(Manifest.Permission.WriteExternalStorage) == Permission.Granted)
                {
                    return true;
                }

                if (ShouldShowRequestPermissionRationale(Manifest.Permission.WriteExternalStorage))
                {
                    Snackbar.Make(FindViewById(Android.Resource.Id.Content),
                                  "GIMME",
                                  Snackbar.LengthIndefinite)
                            .SetAction("OK", delegate { RequestPermissions(PERMISSIONS_TO_REQUEST, requestCode); });
                }
                else
                {
                    RequestPermissions(PERMISSIONS_TO_REQUEST, requestCode);
                }

                return false;
            }

            Log.Warn(TAG, "External storage is not mounted; cannot request permission");
            return false;
        }
    }
   
}
