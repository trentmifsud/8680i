﻿using Android.Bluetooth;
using Android.Content;
using System;

namespace BluetoothDemo.BroadcastReceivers
{
    public class BluetoothReceiver : BroadcastReceiver
    {
        private Muni.Bus bus = MainApp.Bus;
        private DateTime lastMessage = DateTime.Now;
        private TimeSpan ts = TimeSpan.FromSeconds(1);

        public enum BluetoothState { FOUND, CONNECTED, SEARCH_COMPLETE, DISCONNECT_REQUESTED, DISCONNECTED, PAIRED, UNPAIRED, PAIRING };


        public override void OnReceive(Context context, Intent intent)
        {

            ts = DateTime.Now - lastMessage;
            if (ts.TotalSeconds >= 1)
            {
                // report
                string action = intent.Action;
                BluetoothDevice device = (BluetoothDevice)intent.GetParcelableExtra(BluetoothDevice.ExtraDevice);

                if (BluetoothDevice.ActionFound.Equals(action)) //Device found
                {
                    bus.Post(new Models.BluetoothData(device, "Found device", BluetoothState.FOUND));
                }
                else if (BluetoothDevice.ActionAclConnected.Equals(action)) //Device is now connected
                {
                    bus.Post(new Models.BluetoothData(device, "Device Connected", BluetoothState.CONNECTED));
                }
                else if (BluetoothAdapter.ActionDiscoveryFinished.Equals(action))//Done searching
                {
                    bus.Post(new Models.BluetoothData("Search complete", BluetoothState.SEARCH_COMPLETE));
                }
                else if (BluetoothDevice.ActionAclDisconnectRequested.Equals(action)) //Device is about to disconnect
                {
                    bus.Post(new Models.BluetoothData("Disconnect Requested", BluetoothState.DISCONNECT_REQUESTED));
                }
                else if (BluetoothDevice.ActionAclDisconnected.Equals(action))  //Device has disconnected
                {
                    if (device != null)
                    {
                        bus.Post(new Models.BluetoothData(device, "Device disconnected", BluetoothState.DISCONNECTED));
                    }
                    else
                    {
                        bus.Post(new Models.BluetoothData("Device disconnected", BluetoothState.DISCONNECTED));
                    }
                }
                else if (BluetoothDevice.ActionBondStateChanged.Equals(action))
                {

                    if (device != null)
                    {
                        if (device.BondState == Bond.Bonded)
                        {
                            bus.Post(new Models.BluetoothData(device, "Device paired", BluetoothState.PAIRED));
                        }
                        else if (device.BondState == Bond.Bonding)
                        {
                            bus.Post(new Models.BluetoothData(device, "Device pairing", BluetoothState.PAIRING));
                        }
                        else if (device.BondState == Bond.None)
                        {
                            bus.Post(new Models.BluetoothData(device, "Device unpaired", BluetoothState.UNPAIRED));
                        }

                    }
                }
            }
            lastMessage = DateTime.Now;

        }


    }
}