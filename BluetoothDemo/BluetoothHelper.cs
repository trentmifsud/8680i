﻿using Android.Bluetooth;
using Android.Runtime;
using BluetoothDemo.BroadcastReceivers;
using BluetoothDemo.Models;
using Java.IO;
using Java.Lang.Reflect;
using Java.Util;
using Muni;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BluetoothDemo
{
    public class BluetoothHelper : Java.Lang.Object, Android.Bluetooth.IBluetoothProfileServiceListener
    {
        private BluetoothDevice device = null;
        private BluetoothAdapter adapter = BluetoothAdapter.DefaultAdapter;
        // private BluetoothSocket BthSocket = null;
        private BluetoothServerSocket BthServerSocket = null;
        private BluetoothSocket BthSocket = null;

        // SYN M CR
        public readonly byte[] SerialCommandPrefix = new byte[] { 22, 77, 13 };
        public readonly string DeactivateTrigger = "TRGITG1!";
        public readonly string ActivateTrigger = "TRGITG0!";
        public readonly string DefaultScanner = "DEFALT!";
        public readonly string ScannerBeepOne = "BEPEXE1!";
        public readonly string IconBarOn = "GUIICN1!";
        public readonly string IconBarOff = "GUIICN0!";
        public readonly string UnpairWhenCharging = "BT_DSC2!";
        public readonly string ShowSoftwareRevision = "REVINF!";
        public readonly string BattInfo = "BTRINF!";


        //  bluetoothHelper.SendToScanner(bluetoothHelper.DeactivateTrigger, bluetoothHelper.SerialCommandPrefix);
        public readonly string HardwareButtonsOn = "GUIBNE1.";
        public readonly string HardwareButtonsOff = "GUIBNE0.";

        public readonly string UpdateMenu = "GUITXT00";
        public readonly UUID uuid = UUID.FromString("00001101-0000-1000-8000-00805f9b34fb");
        private CancellationTokenSource _ct { get; set; }
        private readonly int sleepTime = 200;
        private readonly bool readAsCharArray = true;

        private Muni.Bus bus = MainApp.Bus;

        public BluetoothHelper()
        {
            MainApp.Bus.Register(this);
        }

        [Subscribe]
        public async void OnSyncComplete(BaseData data)
        {
            if (data is BluetoothData)
            {
                BluetoothData bluetoothData = (BluetoothData)data;
                System.Diagnostics.Debug.WriteLine("Got : " + bluetoothData.BluetoothDevice != null ? bluetoothData.BluetoothDevice.Address : "unknown device");
                if (bluetoothData.BluetoothState == BluetoothReceiver.BluetoothState.CONNECTED)
                {   
                    try
                    {
                       this.BthServerSocket = BluetoothAdapter.DefaultAdapter.ListenUsingInsecureRfcommWithServiceRecord("spp", uuid);
                        
                        this.BthSocket = BthServerSocket.Accept();
                        if (BthSocket != null)
                        {
                            // BEEP!
                            SendToScanner(BthSocket, ScannerBeepOne, SerialCommandPrefix);
                            SendToScanner(ShowSoftwareRevision, SerialCommandPrefix);

                        }
                        if (BthSocket != null && BthSocket.IsConnected)
                        {
                            Task.Run(async () => LoopRead());
                        }
                    }
                    catch(Exception ex)
                    {
                        MainApp.Logger.Error(ex,"Cannot send settings to device");
                    }

                    //if (BthSocket == null || !BthSocket.)
                    //{
                    //    bus.Post(new SyncData("Starting Serial connection"));
                    //    Start("8680i");
                    //}
                    // do in background
                    int worker;
                    int ioCompletion;
                    ThreadPool.GetMaxThreads(out worker, out ioCompletion);
                    //if (worker <= 3)
                   // {
                        ThreadPool.QueueUserWorkItem(o => RemoveOtherBluetoothDevices(bluetoothData.BluetoothDevice));
                   // }
                }
                else if (bluetoothData.BluetoothState == BluetoothReceiver.BluetoothState.DISCONNECTED)
                {
                    // exit gracefully?
                    bus.Post(new BroadcastData("Bluetooth device is disconnected. Cleanup"));
                    Cleanup();
                }
            }
        }

        /// <summary>
        /// Remove older scanners not required.
        /// </summary>
        private void RemoveOtherBluetoothDevices(BluetoothDevice currentDevice)
        {
            try
            {
                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
                ICollection<BluetoothDevice> collection = bluetoothAdapter.BondedDevices;
                
                foreach(BluetoothDevice device in collection)
                {
                    if(!device.Equals(currentDevice))
                    {
                        var mi = device.Class.GetMethod("removeBond", null);
                        mi.Invoke(device, null);
                        MainApp.Logger.Debug("Unpaired : " + device.Address);
                    }
                }
            }
            catch(Exception ex)
            {
                MainApp.Logger.Error(ex, "EXception unpairing device");
            }
        }


        /// <summary>
        /// Start the "reading" loop 
        /// </summary>
        /// <param name="name">Name of the paired bluetooth device (also a part of the name)</param>
        public void Start(string name)
        {
            if (BthSocket == null || !BthSocket.IsConnected)
            {
                BluetoothConnect(name);
            }
            if (BthSocket != null && BthSocket.IsConnected)
            {
                Task.Run(async () => LoopRead());
            }
        }



        public static string MakeUTF8(string paramString)
        {
            string str = null;
            try
            {
                //str = new String(paramString.getBytes("UTF-8"), "UTF-8");

                System.Text.Encoding utf_8 = System.Text.Encoding.UTF8;

                // This is our Unicode string:
                string s_unicode = paramString;

                // Convert a string to utf-8 bytes.
                byte[] utf8Bytes = System.Text.Encoding.UTF8.GetBytes(s_unicode);

                // Convert utf-8 bytes to a string.
                string s_unicode2 = System.Text.Encoding.UTF8.GetString(utf8Bytes);
                return s_unicode2;
            }
            catch (UnsupportedEncodingException)
            {
                // 
            }
            return str;
        }

        public static string ConvertToHexString(string paramString)
        {
            char[] arrayOfChar = paramString.ToCharArray();
            StringBuilder localStringBuffer = new StringBuilder();
            for (int i = 0; i < arrayOfChar.Length; i++)
            {
                localStringBuffer.Append(string.Format("{0:X}", Convert.ToUInt32(arrayOfChar[i])));
            }
            return localStringBuffer.ToString();
        }


        public void UpdateDisplay(string textToSend, byte[] byteStreamHeader)
        {
            try
            {
                if (BthSocket.IsConnected)
                {

                    textToSend = ConvertToHexString(textToSend);
                    //command = "GUITXT004869!";
                    byte[] bytes = Encoding.ASCII.GetBytes(textToSend);
                    byte[] guitxt = Encoding.ASCII.GetBytes(UpdateMenu);
                    byte[] endByte = Encoding.ASCII.GetBytes("!");

                    byte[] sendCommand = byteStreamHeader.Concat(guitxt).Concat(bytes).Concat(endByte).ToArray();
                    BthSocket.OutputStream.Write(sendCommand, 0, sendCommand.Length);


                }
            }
            catch (IOException ex)
            {
                bus.Post(new BroadcastData("ERROR : " + ex.Message));
                MainApp.Logger.Error(ex, "IOException updating display");
            }
            catch (Exception ex)
            {
                MainApp.Logger.Error(ex, "Exception updating display");
            }
        }

        public void SendToScanner(BluetoothSocket socket, string command, byte[] byteStreamHeader)
        {
            try
            {
                if (socket.IsConnected)
                {
                    byte[] bytes = Encoding.ASCII.GetBytes(command);
                    byte[] sendCommand = byteStreamHeader.Concat(bytes).ToArray();
                    socket.OutputStream.Write(sendCommand, 0, sendCommand.Length);


                }
            }
            catch (IOException ex)
            {
                bus.Post(new BroadcastData("ERROR : " + ex.Message));
            }
            catch (Exception)
            {
                // dunno
            }

        }

        public void SendToScanner(string command, byte[] byteStreamHeader)
        {
            try
            {
                if (BthSocket.IsConnected)
                {
                    
                    byte[] bytes = Encoding.ASCII.GetBytes(command);
                    byte[] sendCommand = byteStreamHeader.Concat(bytes).ToArray();
                    BthSocket.OutputStream.Write(sendCommand, 0, sendCommand.Length);


                }
            }
            catch (IOException ex)
            {
                bus.Post(new BroadcastData("ERROR : " + ex.Message));
            }
            catch (Exception)
            {
                // dunno
            }

        }

        private bool isCurrentlyConnecting = false;
        private void BluetoothConnect(string name)
        {
            if (!isCurrentlyConnecting)
            {
                isCurrentlyConnecting = true;
                adapter = BluetoothAdapter.DefaultAdapter;

                if (adapter == null)
                {
                    bus.Post(new BroadcastData("No Bluetooth adapter found."));
                }

                if (!adapter.IsEnabled)
                {
                    bus.Post(new BroadcastData("Bluetooth adapter is not enabled."));
                }

                foreach (BluetoothDevice bd in adapter.BondedDevices)
                {
                    //System.Diagnostics.Debug.WriteLine("Paired devices found: " + bd.Name.ToUpper());
                    MainApp.Logger.Debug("Paired devices found: " + bd.Name.ToUpper());
                    if (bd.Name.ToUpper().IndexOf(name.ToUpper()) >= 0)
                    {

                        //System.Diagnostics.Debug.WriteLine("Found " + bd.Name + ". Try to connect with it!");
                        MainApp.Logger.Debug("Found " + bd.Name + ". Try to connect with it!");
                        device = bd;
                        break;
                    }
                }

                if (device == null)
                {
                    bus.Post(new BroadcastData("Named device not found"));
                }
                else
                {
                    if ((int)Android.OS.Build.VERSION.SdkInt >= 10) // Gingerbread 2.3.3 2.3.4
                    {
                        BthSocket = device.CreateInsecureRfcommSocketToServiceRecord(uuid);
                    }
                    else
                    {
                        BthSocket = device.CreateRfcommSocketToServiceRecord(uuid);
                    }

                    if (BthSocket != null)
                    {
                        try
                        {
                            BthSocket.Connect();
                        }
                        catch (Exception ex)
                        {
                            bus.Post(new BroadcastData("Did not connect!"));
                            MainApp.Logger.Error(ex, "Did not connect!");
                            System.Diagnostics.Debug.WriteLine(ex.ToString());
                        }

                        if (BthSocket.IsConnected)
                        {
                            bus.Post(new BroadcastData("Connected!"));
                        }
                        else
                        {
                            bus.Post(new BroadcastData("Did not connect!"));
                        }


                    }
                }

                isCurrentlyConnecting = false;
            }
        }


        private async Task LoopRead()
        {
            _ct = new CancellationTokenSource();
            while (_ct.IsCancellationRequested == false)
            {
                try
                {
                    Thread.Sleep(sleepTime);
                    adapter = BluetoothAdapter.DefaultAdapter;
                    if (BthSocket.IsConnected)
                    {
                        bus.Post(new BroadcastData("Lets read!"));
                        InputStreamReader mReader = new InputStreamReader(BthSocket.InputStream);
                        BufferedReader buffer = new BufferedReader(mReader);
                        while (_ct.IsCancellationRequested == false)
                        {
                            if (buffer.Ready())
                            {
                                char[] chr = new char[300];
                                string barcode = "";
                                if (readAsCharArray)
                                {
                                    await buffer.ReadAsync(chr);
                                    foreach (char c in chr)
                                    {
                                        if (c == '\0')
                                        {
                                            break;
                                        }

                                        barcode += c;
                                    }
                                }
                                else
                                {
                                    barcode = await buffer.ReadLineAsync();
                                }

                                if (barcode.Length > 0)
                                {
                                    if (barcode.Contains("REVINF"))
                                    {
                                        bus.Post(new _8680BroadcastData(barcode));
                                    }
                                    else
                                    {
                                        bus.Post(new BroadcastData(barcode));
                                    }
                                }
                                // else no-op
                            }
                            else
                            {
                                System.Diagnostics.Debug.WriteLine("No data to read");
                            }

                            // A little stop to the uneverending thread...
                            System.Threading.Thread.Sleep(sleepTime);
                            if (!BthSocket.IsConnected)
                            {
                                System.Diagnostics.Debug.WriteLine("BthSocket.IsConnected = false, Throw exception");
                                MainApp.Logger.Error("BthSocket.IsConnected = false, Throw exception");
                                throw new Exception();
                            }
                        }
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("BthSocket = null");
                        MainApp.Logger.Warn("BthSocket is null");
                    }
                }
                catch (IOException ioe)
                {
                    MainApp.Logger.Error(ioe, "IOException in looped reader");
                    System.Diagnostics.Debug.WriteLine("EXCEPTION: " + ioe.ToString());
                    _ct.Cancel();
                }
                catch (Exception ex)
                {
                    MainApp.Logger.Error(ex, "Exception in looped reader");
                    System.Diagnostics.Debug.WriteLine("EXCEPTION: " + ex.Message);
                    _ct.Cancel();
                }

                finally
                {
                    Cleanup();
                }
            }

            System.Diagnostics.Debug.WriteLine("Exit the external loop");
        }

        private void Cleanup()
        {
            if (BthSocket != null)
            {
                BthSocket.Close();
                BthSocket = null;
            }

            if(this.BthServerSocket != null)
            {
                BthServerSocket.Close();
                BthServerSocket = null;
            }

            device = null;
            adapter = null;

            if (_ct != null)
            {
                _ct.Cancel();
            }
        }

        /// <summary>
        /// Cancel the Reading loop
        /// </summary>
        /// <returns><c>true</c> if this instance cancel ; otherwise, <c>false</c>.</returns>
        public void Cancel()
        {
            Cleanup();  // cancel tasks and reset eveything. 
        }

        public ObservableCollection<string> PairedDevices()
        {
            BluetoothAdapter adapter = BluetoothAdapter.DefaultAdapter;
            ObservableCollection<string> devices = new ObservableCollection<string>();

            foreach (BluetoothDevice bd in adapter.BondedDevices)
            {
                devices.Add(bd.Name);
            }

            return devices;
        }
        public System.Collections.Generic.ICollection<BluetoothDevice> GetBondedDevices()
        {
            try
            {
                return BluetoothAdapter.DefaultAdapter.BondedDevices;
            }
            catch (Exception ex)
            {
                // log this
                throw ex;
            }
        }




        public static string GetBluetoothMacAddress()
        {
            Java.Lang.String bluetoothMacAddress;
            try
            {
                Java.Lang.Class c = Java.Lang.Class.ForName("android.os.SystemProperties");
                Method get = c.GetMethod("get", Java.Lang.Class.FromType(typeof(Java.Lang.String)));
                bluetoothMacAddress = (Java.Lang.String)(get.Invoke(c, "ro.hsm.bt.addr"));
            }
            catch (global::Java.Lang.Exception ex)
            {
                MainApp.Logger.Error(ex, "Exception getting mac address");

                bluetoothMacAddress = new Java.Lang.String("unknown");
            }
            return bluetoothMacAddress.ToString();
        }

        public void OnServiceConnected([GeneratedEnum] ProfileType profile, IBluetoothProfile proxy)
        {
            System.Diagnostics.Debug.WriteLine("OnServiceConnected");
        }

        public void OnServiceDisconnected([GeneratedEnum] ProfileType profile)
        {
            System.Diagnostics.Debug.WriteLine("OnServiceDisconnected");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Cleanup();
        }
    }
}