ao.load = function (path) {};
ao.print = function (s) {
	var div = document.getElementById('\x6f\x75\x74\x70\x75\x74');
	if (div)
		div.innerHTML += s.replace(/&/g, '\x26\x61\x6d\x70\x3b').replace(/</g, '\x26\x6c\x74\x3b') + '\x3c\x62\x72\x3e';
};
ao.debug = function (s) {
	return;
	var div = document.getElementById('\x64\x65\x62\x75\x67');
	if (div)
		div.innerHTML += s + '\r\n';
};
(function () {
	var head = document.getElementsByTagName('\x68\x65\x61\x64')[0];
	function fL(s) {
		var tag = document.createElement('\x73\x63\x72\x69\x70\x74');
		tag.type = '\x74\x65\x78\x74\x2f\x6a\x61\x76\x61\x73\x63\x72\x69\x70\x74';
		tag.src = s + '\x2e\x6a\x73';
		head.appendChild(tag);
	};
	for (var i = 0; i < symdesc.length; i++) {
		if (!symdesc[i])
			continue;
		fL('\x62\x77\x69\x70\x70\x2f' + symdesc[i].sym);
	}
	fL('\x62\x77\x69\x70\x70\x2f\x72\x65\x6e\x6c\x69\x6e\x65\x61\x72');
	fL('\x62\x77\x69\x70\x70\x2f\x72\x65\x6e\x6d\x61\x74\x72\x69\x78');
	fL('\x62\x77\x69\x70\x70\x2f\x72\x65\x6e\x6d\x61\x78\x69\x6d\x61\x74\x72\x69\x78');
	for (var x = 1; x <= 10; x++) {
		fL('\x66\x6f\x6e\x74\x73\x2f\x6f\x63\x72\x62\x31\x30\x2d' + (x < 10 ? '\x30' : '') + x);
		fL('\x66\x6f\x6e\x74\x73\x2f\x6f\x63\x72\x62\x31\x32\x2d' + (x < 10 ? '\x30' : '') + x);
	}
})();
function rd() {
	var clr = [0, 0, 0];
	var pts = [];
	var minx = Infinity;
	var miny = Infinity;
	var maxx = 0;
	var maxy = 0;
	this.color = function (r, g, b) {
		clr = [r, g, b];
	};
	this.set = function (x, y) {
		x = Math.floor(x);
		y = Math.floor(y);
		pts.push([x, y, clr]);
		if (minx > x)
			minx = x;
		if (miny > y)
			miny = y;
		if (maxx < x)
			maxx = x;
		if (maxy < y)
			maxy = y;
	};
	this.show = function (cvsid, rot, type, scalingFactor, barcodeCommandShow) {
		var cvs = document.getElementById(cvsid);
		if (pts.length == 0) {
			cvs.width = 32;
			cvs.height = 32;
			cvs.getContext('\x32\x64').clearRect(0, 0, cvs.width, cvs.height);
			cvs.style.visibility = '\x76\x69\x73\x69\x62\x6c\x65';
			return;
		}
		if (rot == '\x52' || rot == '\x4c') {
			var h = maxx - minx + 1;
			var w = maxy - miny + 1;
		} else {
			var w = maxx - minx + 1;
			var h = maxy - miny + 1;
		}
		if (type == '\x32\x44' && barcodeCommandShow == true) {
			cvs.width = w;
			if (scalingFactor > 1)
				cvs.height = h + 10 * scalingFactor;
			else
				cvs.height = h + 20;
		} else {
			cvs.width = w;
			cvs.height = h;
		}
		var ctx = cvs.getContext('\x32\x64');
		ctx.fillStyle = '\x23\x66\x66\x66';
		ctx.fillRect(0, 0, cvs.width, cvs.height);
		ctx.fillStyle = '\x23\x30\x30\x30';
		var id = ctx.getImageData(0, 0, cvs.width, cvs.height);
		var dat = id.data;
		for (var i = 0; i < pts.length; i++) {
			var x = pts[i][0] - minx;
			var y = pts[i][1] - miny;
			var c = pts[i][2];
			if (rot == '\x4e') {
				y = h - y - 1;
			} else if (rot == '\x49') {
				x = w - x - 1;
			} else {
				y = w - y;
				if (rot == '\x4c') {
					var t = y;
					y = h - x - 1;
					x = t - 1;
				} else {
					var t = x;
					x = w - y;
					y = t;
				}
			}
			var idx = (y * id.width + x) * 4;
			dat[idx++] = c[0];
			dat[idx++] = c[1];
			dat[idx++] = c[2];
			dat[idx] = 255;
		}
		ctx.putImageData(id, 0, 0);
		cvs.style.visibility = '\x76\x69\x73\x69\x62\x6c\x65';
	}
}
