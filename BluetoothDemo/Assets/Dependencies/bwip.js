function ao() {
	this.ptr = 0;
	this.stk = [];
	this.dict = {};
	this.dstk = [this.dict];
	this.gstk = [];
	this.bmap = null;
	this.dstk.get = function (id) {
		for (var i = this.length - 1; i >= 0; i--) {
			if (this[i][id] !== undefined) {
				return this[i][id];
			}
		}
	};
	this.greset();
};
ao.bwipp = {};
ao.fonts = {};
ao.print = function (s) {};
ao.debug = function (s) {};
ao.psarray = function (v) {
	if (!(this instanceof ao.psarray))
		return new ao.psarray(v);
	if (typeof(v) == "\x6e\x75\x6d\x62\x65\x72") {
		var a = [];
		for (var i = 0; i < v; i++)
			a[i] = null;
		this.value = a;
		this.length = v;
		this.offset = 0;
	} else if (v instanceof Array) {
		this.value = v;
		this.offset = 0;
		this.length = v.length;
	} else {
		this.value = [];
		this.length = v.length;
		this.offset = 0;
		for (var i = 0; i < v.length; i++)
			this.value[i] = v.value[v.offset + i];
	}
	for (var i = 0; i < this.length; i++)
		this[i] = undefined;
};
ao.psarray.prototype.toString = function () {
	var s = '';
	for (var i = this.offset; i < this.offset + this.length; i++)
		s += '\x20' + ao.pstostring(this.value[i]);
	return '\x5b' + s.substr(1) + '\x5d';
};
ao.psarray.prototype.valueOf = function () {
	var s = '';
	for (var i = this.offset; i < this.offset + this.length; i++)
		s += '\x20' + ao.pstostring(this.value[i]);
	return '\x5b' + s.substr(1) + '\x5d';
};
ao.psarray.prototype.get = function (n) {
	return this.value[this.offset + parseFloat(n)];
};
ao.psarray.prototype.set = function (n, v) {
	this.value[this.offset + parseFloat(n)] = v;
};
ao.psarray.prototype.subset = function (offset, length) {
	if (isNaN(length) || offset + length > this.length)
		length = this.length - offset;
	var copy = new ao.psarray(length);
	copy.value = this.value;
	copy.offset = this.offset + offset;
	return copy;
};
ao.psarray.prototype.assign = function (offset, source) {
	if (source instanceof Array) {
		if (this.length == this.value.length && this.length == source.length)
			this.value = source;
		else
			for (var i = 0; i < source.length; i++)
				this.value[this.offset + offset + i] = source[i];
	} else
		for (var i = 0; i < source.length; i++)
			this.value[this.offset + offset + i] = source.value[source.offset + i];
};
ao.psstring = function (v) {
	if (!(this instanceof ao.psstring))
		return new ao.psstring(v);
	if (typeof(v) == '\x6e\x75\x6d\x62\x65\x72') {
		this.value = [];
		this.length = v;
		this.offset = 0;
		for (var i = 0; i < v; i++)
			this.value[i] = 0;
	} else if (typeof(v) == '\x73\x74\x72\x69\x6e\x67') {
		this.value = [];
		this.length = v.length;
		this.offset = 0;
		for (var i = 0; i < v.length; i++)
			this.value[i] = v.charCodeAt(i);
	} else {
		this.value = [];
		this.length = v.length;
		this.offset = 0;
		for (var i = 0; i < v.length; i++)
			this.value[i] = v.value[v.offset + i];
	}
	for (var i = 0; i < this.length; i++)
		this[i] = NaN;
};
ao.psstring.prototype.toString = function () {
	var s = '';
	for (var i = this.offset; i < this.offset + this.length; i++)
		s += String.fromCharCode(this.value[i]);
	return s;
};
ao.psstring.prototype.valueOf = function () {
	var s = '';
	for (var i = this.offset; i < this.offset + this.length; i++)
		s += String.fromCharCode(this.value[i]);
	return s;
};
ao.psstring.prototype.get = function (n) {
	return this.value[this.offset + parseFloat(n)];
};
ao.psstring.prototype.set = function (n, v) {
	this.value[this.offset + parseFloat(n)] = v;
};
ao.psstring.prototype.subset = function (offset, length) {
	if (isNaN(length) || offset + length > this.length)
		length = this.length - offset;
	var copy = new ao.psstring(length);
	copy.value = this.value;
	copy.offset = this.offset + offset;
	return copy;
};
ao.psstring.prototype.assign = function (offset, source) {
	if (typeof(source) === '\x73\x74\x72\x69\x6e\x67') {
		for (var i = 0; i < source.length; i++)
			this.value[this.offset + offset + i] = source.charCodeAt(i);
	} else {
		for (var i = 0; i < source.length; i++)
			this.value[this.offset + offset + i] = source.value[source.offset + i];
	}
};
ao.psstring.prototype.indexOf = function (s) {
	return this.toString().indexOf(s.toString());
};
ao.pstype = function (v) {
	if (v === null || v === undefined)
		return '\x6e\x75\x6c\x6c\x74\x79\x70\x65';
	var t = typeof(v);
	if (t == '\x6e\x75\x6d\x62\x65\x72')
		return v % 1 ? '\x72\x65\x61\x6c\x74\x79\x70\x65' : '\x69\x6e\x74\x65\x67\x65\x72\x74\x79\x70\x65';
	if (t == '\x62\x6f\x6f\x6c\x65\x61\x6e')
		return '\x62\x6f\x6f\x6c\x65\x61\x6e\x74\x79\x70\x65';
	if (v instanceof ao.psarray)
		return '\x61\x72\x72\x61\x79\x74\x79\x70\x65';
	if (v instanceof ao.psstring)
		return '\x73\x74\x72\x69\x6e\x67\x74\x79\x70\x65';
	return '\x64\x69\x63\x74\x74\x79\x70\x65';
};
ao.pstostring = function (v) {
	if (v === null)
		return '\x6e\x75\x6c\x6c';
	if (typeof(v) == '\x66\x75\x6e\x63\x74\x69\x6f\x6e')
		return '\x2d\x2d\x66\x75\x6e\x63\x74\x69\x6f\x6e\x2d\x2d';
	if (v instanceof ao.psarray)
		return v.toString();
	if (v instanceof ao.psstring) {
		var s = '\x28';
		for (var i = 0; i < v.length; i++) {
			var cd = v.value[v.offset + i];
			switch (cd) {
			case 92:
				s += '\\\\';
				break;
			case 10:
				s += '\\\x6e';
				break;
			case 13:
				s += '\\\x72';
				break;
			case 9:
				s += '\\\x74';
				break;
			case 8:
				s += '\\\x62';
				break;
			case 40:
				s += '\\\x28';
				break;
			case 41:
				s += '\\\x29';
				break;
			default:
				if (cd < 32 || cd > 127)
					s += '\\' + (function (s) {
						return '\x30\x30\x30'.substr(s.length) + s;
					})(cd.toString(8));
				else
					s += String.fromCharCode(cd);
			}
		}
		return s + '\x29';
	}
	if (typeof(v) == '\x6f\x62\x6a\x65\x63\x74') {
		var s = '';
		for (var i in v)
			s += '\x20\x2f' + i + '\x20' + ao.pstostring(v[i]);
		return '\x3c\x3c' + s + '\x20\x3e\x3e';
	}
	if (typeof(v) == '\x6e\x75\x6d\x62\x65\x72' && v % 1) {
		return v.toPrecision(12).replace(/0*$/, '');
	}
	return '' + v;
};
ao.prototype.bitmap = function (bmap) {
	if (!bmap)
		return this.bmap;
	this.bmap = bmap;
};
ao.prototype.value = function (v) {
	if (v === true || v === false || v === null)
		return v;
	var t = typeof(v);
	if (t == '\x6e\x75\x6d\x62\x65\x72')
		return v;
	if (t == '\x73\x74\x72\x69\x6e\x67')
		return ao.psstring(v);
	if (v instanceof Array)
		return ao.psarray(v);
	return v;
};
ao.prototype.push = function (v) {
	this.stk[this.ptr++] = this.value(v);
};
ao.prototype.pop = function () {
	if (this.ptr <= 0)
		throw '\x2d\x2d\x75\x6e\x64\x65\x72\x66\x6c\x6f\x77\x2d\x2d';
	return this.stk[--this.ptr];
};
ao.prototype.call = function (name) {
	if (!ao.bwipp[name])
		ao.load('\x62\x77\x69\x70\x70\x2f' + name + '\x2e\x6a\x73');
	if (!ao.bwipp[name])
		throw name + '\x3a\x20\x2d\x2d\x75\x6e\x64\x65\x66\x69\x6e\x65\x64\x2d\x2d';
	this.dict[name] = ao.bwipp[name];
	ao.bwipp[name].call(this);
};
ao.prototype.eval = function (src) {
	src = src.toString();
	if (!/^<(([0-9A-F][0-9A-F])*)>$/i.test(src))
		throw '\x65\x76\x61\x6c\x3a\x20\x6e\x6f\x74\x20\x61\x20\x68\x65\x78\x20\x73\x74\x72\x69\x6e\x67\x20\x6c\x69\x74\x65\x72\x61\x6c';
	var dst = new ao.psstring((src.length - 2) / 2);
	var idx = 0;
	for (var i = 1; i < src.length - 1; i += 2)
		dst.set(idx++, parseInt(src.substr(i, 2), 16));
	this.stk[this.ptr++] = dst;
};
ao.prototype.greset = function () {
	this.g_tdx = 0;
	this.g_tdy = 0;
	this.g_tsx = 1;
	this.g_tsy = 1;
	this.g_posx = 0;
	this.g_posy = 0;
	this.g_penw = 1;
	this.g_path = [];
	this.g_font = null;
	this.g_rgb = [0, 0, 0];
};
ao.prototype.currentpoint = function () {
	return {
		x : (this.g_posx - this.g_tdx) / this.g_tsx,
		y : (this.g_posy - this.g_tdy) / this.g_tsy
	};
};
ao.prototype.currentfont = function () {
	return this.g_font;
};
ao.prototype.findfont = function (name) {
	return {
		FontName : name
	};
};
ao.prototype.dtransform = function (mtx, dx, dy) {
	return {
		dx : dx,
		dy : dy
	};
};
ao.prototype.translate = function (x, y) {
	this.g_tdx = this.g_tsx * x;
	this.g_tdy = this.g_tsy * y;
};
ao.prototype.scale = function (x, y) {
	this.g_tsx *= x;
	this.g_tsy *= y;
};
ao.prototype.setlinewidth = function (w) {
	this.g_penw = w;
};
ao.prototype.setfont = function (f) {
	this.g_font = f;
};
ao.prototype.setrgb = function (r, g, b) {
	var r = Math.round(r * 255);
	var g = Math.round(g * 255);
	var b = Math.round(b * 255);
	this.bmap.color(r, g, b);
	this.g_rgb = [r, g, b];
};
ao.prototype.setcmyk = function (c, m, y, k) {
	var r = Math.round((1 - c) * (1 - k) * 255);
	var g = Math.round((1 - m) * (1 - k) * 255);
	var b = Math.round((1 - y) * (1 - k) * 255);
	this.bmap.color(r, g, b);
	this.g_rgb = [r, g, b];
};
ao.prototype.newpath = function () {
	this.g_path = [];
};
ao.prototype.closepath = function () {
	if (this.g_path.length) {
		var c0 = this.g_path[0];
		var c1 = this.g_path[this.g_path.length - 1];
		this.g_path.push([c1[0], c1[1]]);
		this.g_path.push(['\x63']);
		this.g_path.push([c0[0], c0[1]]);
	}
};
ao.prototype.moveto = function (x, y) {
	this.g_posx = this.g_tdx + this.g_tsx * x;
	this.g_posy = this.g_tdy + this.g_tsy * y;
	ao.debug('\x6d\x6f\x76\x65\x74\x6f\x3a\x20\x70\x6f\x73\x78\x2c\x70\x6f\x73\x79\x3d\x28' + this.g_posx + '\x2c' + this.g_posy + '\x29');
};
ao.prototype.rmoveto = function (x, y) {
	this.g_posx += this.g_tsx * x;
	this.g_posy += this.g_tsy * y;
	ao.debug('\x72\x6d\x6f\x76\x65\x74\x6f\x3a\x20\x70\x6f\x73\x78\x2c\x70\x6f\x73\x79\x3d\x28' + this.g_posx + '\x2c' + this.g_posy + '\x29');
};
ao.prototype.lineto = function (x, y) {
	this.g_path.push([this.g_posx, this.g_posy]);
	this.g_path.push(['\x6c']);
	this.g_posx = this.g_tdx + this.g_tsx * x;
	this.g_posy = this.g_tdy + this.g_tsy * y;
	this.g_path.push([this.g_posx, this.g_posy]);
	ao.debug('\x6c\x69\x6e\x65\x74\x6f\x3a\x20\x70\x6f\x73\x78\x2c\x70\x6f\x73\x79\x3d\x28' + this.g_posx + '\x2c' + this.g_posy + '\x29');
};
ao.prototype.rlineto = function (x, y) {
	this.g_path.push([this.g_posx, this.g_posy]);
	this.g_path.push(['\x6c']);
	this.g_posx += this.g_tsx * x;
	this.g_posy += this.g_tsy * y;
	this.g_path.push([this.g_posx, this.g_posy]);
	ao.debug('\x72\x6c\x69\x6e\x65\x74\x6f\x3a\x20\x70\x6f\x73\x78\x2c\x70\x6f\x73\x79\x3d\x28' + this.g_posx + '\x2c' + this.g_posy + '\x29');
};
ao.prototype.arc = function (x, y, r, sa, ea, ccw) {
	if (sa == ea)
		return;
	if (sa != 0 && sa != 360 || ea != 0 && ea != 360)
		throw '\x61\x72\x63\x3a\x20\x6e\x6f\x74\x20\x61\x20\x66\x75\x6c\x6c\x20\x63\x69\x72\x63\x6c\x65\x20\x28' + sa + '\x2c' + ea + '\x29';
	x = this.g_tdx + this.g_tsx * x;
	y = this.g_tdy + this.g_tsy * y;
	var rx = r * this.g_tsx;
	var ry = r * this.g_tsy;
	this.g_path.push([x - rx, y - ry]);
	this.g_path.push(['\x61', {
				x : x,
				y : y,
				rx : rx,
				ry : ry,
				sa : sa,
				ea : ea,
				ccw : ccw
			}
		]);
	this.g_path.push([x + rx, y + ry]);
};
ao.prototype.getfont = function () {
	var fs = Math.floor(this.g_tsx);
	if (fs < 1)
		fs = 1;
	else if (fs > 10)
		fs = 10;
	var key = this.g_font.FontSize + (fs < 10 ? '\x2d\x30' : '\x2d') + fs;
	if (!ao.fonts.OCRB || !ao.fonts.OCRB[key])
		ao.load('\x66\x6f\x6e\x74\x73\x2f\x6f\x63\x72\x62' + key + '\x2e\x6a\x73');
	return !ao.fonts.OCRB ? undefined : ao.fonts.OCRB[key];
};
ao.prototype.stringwidth = function (str) {
	var fn = this.getfont();
	if (!fn)
		return {
			w : 0,
			h : 0
		};
	var w = 0,
	a = 0,
	d = 0;
	for (var i = 0; i < str.length; i++) {
		var ch = String.fromCharCode(str.get(i));
		var g = fn.g[ch];
		if (!g) {
			w += fn.w;
		} else {
			w += Math.max(g.l + g.w, fn.w);
			if (g.t < 0) {
				if (d < g.h - g.t)
					d = g.h - g.t;
			} else {
				if (a < g.t)
					a = g.t;
				if (d < g.h - g.t)
					d = g.h - g.t;
			}
		}
	}
	w += (str.length - 1) * Math.floor(fn.w / 4);
	return {
		w : w / this.g_tsx,
		h : (a + d) / this.g_tsy
	};
};
ao.prototype.charpath = function (str, b) {
	var sw = this.stringwidth(str);
	this.rlineto(sw.w, 0);
	this.rlineto(0, sw.h);
	this.rlineto(-sw.w, 0);
};
ao.prototype.pathbbox = function () {
	if (!this.g_path.length)
		throw '\x70\x61\x74\x68\x62\x62\x6f\x78\x3a\x20\x2d\x2d\x6e\x6f\x63\x75\x72\x72\x65\x6e\x74\x70\x6f\x69\x6e\x74\x2d\x2d';
	var pth = this.g_path;
	var llx = pth[0][0];
	var lly = pth[0][1];
	var urx = 0;
	var ury = 0;
	for (var i = 2, inc = 2; i < pth.length; i += inc) {
		if (llx > pth[i][0])
			llx = pth[i][0];
		if (urx < pth[i][0])
			urx = pth[i][0];
		if (lly > pth[i][1])
			lly = pth[i][1];
		if (ury < pth[i][1])
			ury = pth[i][1];
		inc = (inc == 2 ? 1 : 2);
	}
	return {
		llx : (llx - this.g_tdx) / this.g_tsx,
		lly : (lly - this.g_tdy) / this.g_tsy,
		urx : (urx - this.g_tdx) / this.g_tsx,
		ury : (ury - this.g_tdy) / this.g_tsy
	};
};
ao.prototype.gsave = function () {
	var ctx = {};
	for (id in this)
		if (id.indexOf('\x67\x5f') == 0)
			ctx[id] = this.gclone(this[id]);
	this.gstk.push(ctx);
};
ao.prototype.grestore = function () {
	if (!this.gstk.length)
		throw '\x67\x72\x65\x73\x74\x6f\x72\x65\x3a\x20\x73\x74\x61\x63\x6b\x20\x75\x6e\x64\x65\x72\x66\x6c\x6f\x77';
	var ctx = this.gstk.pop();
	for (id in ctx)
		this[id] = ctx[id];
	this.bmap.color(this.g_rgb[0], this.g_rgb[1], this.g_rgb[2]);
};
ao.prototype.stroke = function () {
	var penx = this.g_penw * this.g_tsx;
	var peny = this.g_penw * this.g_tsy;
	var segs = this.g_path.length / 3;
	if (this.g_path[this.g_path.length - 2][0] == '\x63')
		segs--;
	for (var i = 0; i < this.g_path.length; ) {
		var s = this.g_path[i++];
		var a = this.g_path[i++];
		var e = this.g_path[i++];
		switch (a[0]) {
		case '\x6c':
			this.drawline(true, s[0], s[1], e[0], e[1], penx, peny, segs > 1);
			break;
		case '\x61':
			this.drawarc(s[0], s[1], e[0], e[1], a[1].sa, a[1].se, penx, peny);
			break;
		case '\x63':
			break;
		default:
			throw '\x73\x74\x72\x6f\x6b\x65\x3a\x20\x75\x6e\x64\x65\x66\x69\x6e\x65\x64\x20\x6f\x70\x63\x6f\x64\x65\x3a\x20' + a[0];
		}
	}
	this.g_path = [];
};
ao.prototype.fill = function () {
	if (!this.g_path.length)
		return;
	if (this.g_path[this.g_path.length - 2][0] != '\x63')
		this.closepath();
	var bmap = this.bmap;
	this.bmap = new ao.fillmap;
	for (var i = 0; i < this.g_path.length; ) {
		var s = this.g_path[i++];
		var a = this.g_path[i++];
		var e = this.g_path[i++];
		switch (a[0]) {
		case '\x6c':
			this.drawline(false, s[0], s[1], e[0], e[1], 1, 1);
			break;
		case '\x61':
			this.drawarc(s[0], s[1], e[0], e[1], a[1].sa, a[1].se, 1, 1);
			break;
		case '\x63':
			this.bmap.fill();
			break;
		default:
			throw '\x66\x69\x6c\x6c\x3a\x20\x75\x6e\x64\x65\x66\x69\x6e\x65\x64\x20\x6f\x70\x63\x6f\x64\x65\x3a\x20' + a[0];
		}
	}
	this.bmap.xfer(bmap);
	this.bmap = bmap;
	this.g_path = [];
};
ao.prototype.imagemask = function (width, height, polarity, matrix, source) {
	var ma = matrix.get(0);
	var mb = matrix.get(1);
	var mc = matrix.get(2);
	var md = matrix.get(3);
	var mx = matrix.get(4);
	var my = matrix.get(5);
	var sx = this.g_tsx;
	var sy = this.g_tsy;
	var w2 = width * width;
	var h2 = height * height;
	var da = (ma < 0 ? 1 : 0);
	var db = (mb < 0 ? 1 : 0);
	var dc = (mc < 0 ? 1 : 0);
	var dd = (md < 0 ? 1 : 0);
	var rl = Math.ceil(width / 8);
	for (var y = 0; y < height; y++) {
		for (var x = 0; x < width; x++) {
			var by = source.get(y * rl + Math.floor(x / 8));
			var bt = by & (1 << 7 - (x % 8));
			if (bt && !polarity || !bt && polarity)
				continue;
			var x0 = Math.floor(this.g_tdx + ((x + da - mx) * ma + (y + dc - my) * mc) * sx / w2);
			var y0 = Math.floor(this.g_tdy + ((y + dd - my) * md + (x + db - mx) * mb) * sy / h2);
			var x1 = Math.floor(x0 + sx / width);
			var y1 = Math.floor(y0 + sy / height);
			for (var j = y0; j < y1; j++) {
				for (var i = x0; i < x1; i++)
					this.bmap.set(i, j);
			}
		}
	}
};
ao.prototype.show = function (str, dx, dy) {
	var fn = this.getfont();
	if (!fn)
		return;
	dx = this.g_tsx * dx;
	dy = this.g_tsy * dy;
	for (var i = 0; i < str.length; i++) {
		var ch = String.fromCharCode(str.get(i));
		var g = fn.g[ch];
		if (!g) {
			this.g_posx += fn.w + Math.floor(fn.w / 4) + dx;
			continue;
		}
		var bm = g.m;
		var l = this.g_posx + g.l;
		var t = this.g_posy + g.t + dy - 2;
		var e = bm.charAt(0);
		var tmin = t - g.h;
		var lmax = l + g.w;
		var bx = 1;
		var by;
		var rl;
		if (e == '\x62') {
			for (var y = t; y > tmin; y--)
				for (var x = 0; x < g.w; x++) {
					if (!(x % 8)) {
						by = (parseInt(bm.charAt(bx), 16) << 4) | parseInt(bm.charAt(bx + 1), 16);
						bx += 2;
					}
					if (by & 0x80)
						this.bmap.set(l + x, y);
					by <<= 1;
				}
		} else if (e == '\x78') {
			for (var y = t; y > tmin; y--) {
				for (var x = l; x < lmax; ) {
					by = (parseInt(bm.charAt(bx), 16) << 4) | parseInt(bm.charAt(bx + 1), 16);
					rl = by >> 1;
					bx += 2;
					if (by & 1) {
						while (rl--)
							this.bmap.set(x++, y);
					} else
						x += rl;
				}
			}
		} else if (e == '\x79') {
			for (var x = l; x < lmax; x++) {
				for (var y = t; y > tmin; ) {
					by = (parseInt(bm.charAt(bx), 16) << 4) | parseInt(bm.charAt(bx + 1), 16);
					rl = by >> 1;
					bx += 2;
					if (by & 1) {
						while (rl--)
							this.bmap.set(x, y--);
					} else
						y -= rl;
				}
			}
		} else
			throw '\x75\x6e\x6b\x6e\x6f\x77\x6e\x20\x66\x6f\x6e\x74\x20\x62\x69\x74\x6d\x61\x70\x20\x65\x6e\x63\x6f\x64\x69\x6e\x67\x3a\x20' + e;
		this.g_posx += Math.max(g.l + g.w, fn.w) + Math.floor(fn.w / 4) + dx;
	}
};
ao.prototype.gclone = function (o) {
	if (o instanceof Array) {
		var t = [];
		for (var i = 0; i < o.length; i++)
			t[i] = this.gclone(o[i]);
		return t;
	}
	if (o instanceof Object) {
		var t = {};
		for (i in o)
			t[i] = this.gclone(o[i]);
		return t;
	}
	return o;
};
ao.prototype.drawline = function (optmz, x1, y1, x2, y2, penx, peny, merge) {
	if (optmz && (x1 == x2 || y1 == y2)) {
		var lx = Math.round(penx);
		var ly = Math.round(peny);
		if (y2 < y1) {
			var t = y1;
			y1 = y2;
			y2 = t;
		}
		if (x2 < x1) {
			var t = x1;
			x1 = x2;
			x2 = t;
		}
		if (x1 == x2) {
			x1 = Math.round(x1 - lx / 2);
			x2 = Math.round(x2 + lx / 2);
			y1 = Math.round(y1 - (merge ? ly / 2 : 0));
			y2 = Math.round(y2 + (merge ? ly / 2 : 0));
		} else {
			y1 = Math.round(y1 - ly / 2);
			y2 = Math.round(y2 + ly / 2);
			x1 = Math.round(x1 - (merge ? lx / 2 : 0));
			x2 = Math.round(x2 + (merge ? lx / 2 : 0));
		}
		for (var y = y1; y < y2; y++)
			for (var x = x1; x < x2; x++)
				this.bmap.set(x, y);
		return;
	}
	x1 = Math.floor(x1);
	x2 = Math.floor(x2);
	y1 = Math.floor(y1);
	y2 = Math.floor(y2);
	var du = Math.abs(x2 - x1);
	var dv = Math.abs(y2 - y1);
	var kx = (x2 < x1 ? -1 : 1);
	var ky = (y2 < y1 ? -1 : 1);
	var x = x1;
	var y = y1;
	var d = 0;
	var penw = Math.floor(Math.sqrt(penx * penx + peny * peny));
	var pixh = Math.round(Math.sqrt((penw * penw) / ((dv * dv) / (du * du) + 1))) || 1;
	var pixw = Math.round(Math.sqrt(penw * penw - pixh * pixh)) || 1;
	if (du >= dv) {
		while (x != x2) {
			for (var j = 0; j < pixh; j++)
				this.bmap.set(x, y + j);
			d += dv;
			if (d >= du) {
				d -= du;
				y += ky;
			}
			x += kx;
		}
		for (var j = 0; j < pixh; j++)
			this.bmap.set(x, y + j);
	} else {
		while (y != y2) {
			for (var j = 0; j < pixw; j++)
				this.bmap.set(x + j, y);
			d += du;
			if (d >= dv) {
				d -= dv;
				x += kx;
			}
			y += ky;
		}
		for (var j = 0; j < pixw; j++)
			this.bmap.set(x + j, y);
	}
};
ao.prototype.drawarc = function (x0, y0, x1, y1, sa, se, penx, peny) {
	var a = Math.abs(x1 - x0);
	var b = Math.abs(y1 - y0);
	var b1 = b & 1;
	var dx = 4 * (1 - a) * b * b;
	var dy = 4 * (b1 + 1) * a * a;
	var err = dx + dy + b1 * a * a;
	var e2;
	if (x0 > x1) {
		x0 = x1;
		x1 += a;
	}
	if (y0 > y1)
		y0 = y1;
	y0 += Math.floor((b + 1) / 2);
	y1 = y0 - b1;
	a *= 8 * a;
	b1 = 8 * b * b;
	do {
		this.bmap.set(x1, y0);
		this.bmap.set(x0, y0);
		this.bmap.set(x0, y1);
		this.bmap.set(x1, y1);
		e2 = 2 * err;
		if (e2 >= dx) {
			x0++;
			x1--;
			dx += b1;
			err += dx;
		}
		if (e2 <= dy) {
			y0++;
			y1--;
			dy += a;
			err += dy;
		}
	} while (x0 <= x1);
	while (y0 - y1 < b) {
		this.bmap.set(x0 - 1, y0);
		this.bmap.set(x1 + 1, y0++);
		this.bmap.set(x0 - 1, y1);
		this.bmap.set(x1 + 1, y1--);
	}
};
ao.fillmap = function () {
	var bmap = [];
	var minx = Infinity;
	var miny = Infinity;
	var maxx = -Infinity;
	var maxy = -Infinity;
	this.set = function (x, y) {
		x = ~~x;
		y = ~~y;
		if (!bmap[y])
			bmap[y] = [];
		bmap[y][x] = 0;
		if (minx > x)
			minx = x;
		if (maxx < x)
			maxx = x;
		if (miny > y)
			miny = y;
		if (maxy < y)
			maxy = y;
	};
	function get(x, y) {
		return bmap[y][x];
	};
	function fi(x, y) {
		bmap[y][x] = bmap[y][x] == 1 ? undefined : 1;
	};
	this.fill = function () {
		var x0 = Math.floor(minx + (maxx - minx) / 2);
		var y0 = Math.floor(miny + (maxy - miny) / 2);
		for (var y = y0; y <= maxy; y++) {
			for (var t0 = minx; t0 <= maxx && get(t0, y) !== 0; t0++);
			for (var t1 = maxx; t1 >= minx && get(t1, y) !== 0; t1--);
			while (t0 <= t1)
				fi(t0++, y);
		}
		for (var y = y0 - 1; y >= miny; y--) {
			for (var t0 = minx; t0 <= maxx && get(t0, y) !== 0; t0++);
			for (var t1 = maxx; t1 >= minx && get(t1, y) !== 0; t1--);
			while (t0 <= t1)
				fi(t0++, y);
		}
	};
	this.xfer = function (bmap) {
		var x0 = minx;
		var x1 = maxx;
		var y0 = miny;
		var y1 = maxy;
		var out = '';
		for (var y = y0; y <= y1; y++) {
			for (var x = x0; x <= x1; x++) {
				if (get(x, y) === 1)
					bmap.set(x, y);
				out += get(x, y) === 1 ? '\x58' : '\x30';
			}
			out += '\r\n';
		}
	}
}
