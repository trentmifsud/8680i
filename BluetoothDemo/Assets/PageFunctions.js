function reset()
{
    document.getElementById("description").innerHTML = "";
    var div = document.getElementById("thediv1");
    var canvas = document.getElementById("thecanvas1");
    div.removeChild(canvas);

    document.getElementById("description2").innerHTML = "";
    var div = document.getElementById("thediv2");
    var canvas2 = document.getElementById("thecanvas2");
    div.removeChild(canvas2);
}
function HsmCreateAztecOne(barcodeValue, text) {
    document.getElementById("description").innerHTML = "<font size=1 color=#DCDCDC>" + barcodeValue + "</font><br><br>" + text;
    var div = document.getElementById("thediv");
    HsmCreateOneAztec(barcodeValue, 'thediv', 'thecanvas');
    var img = getImageFromCanvas("thecanvas");
    var canvas = document.getElementById("thecanvas");
    div.removeChild(canvas);
    p = document.createElement("p");
    p.align="center";
    p.appendChild(img);
    div.insertBefore(p, div.firstChild);
}
function HsmCreateAztec(barcodeValue) {
    var div = document.getElementById("thediv");
    div.style.display = "none"; //%DF mod for EWR-27735 Android 8670 Bluetooth Pairing Application enhancement
    HsmCreateAztec_QuickConnect(barcodeValue);
}
function HsmCreateAztec_QuickConnect(barcodeVal) {
    document.getElementById("description2").innerHTML = "<font size=1 color=#DCDCDC>" + barcodeVal + "</font><br><br>Scan to Link"; //%DF mod for EWR-27735 Android 8670 Bluetooth Pairing Application enhancement
    var div = document.getElementById("thediv2");
    HsmCreateOneAztec(barcodeVal, 'thediv2', 'thecanvas2');
    var img = getImageFromCanvas("thecanvas2");
    var canvas = document.getElementById("thecanvas2");
    div.removeChild(canvas);
    p = document.createElement("p");
    p.align="center";
    p.appendChild(img);
    div.insertBefore(p, div.firstChild);
}
function HsmCreateOneAztec(barcodeValue, thediv_str, thecanvas_str) {
    var canvasId = thecanvas_str;
    var canvas = document.getElementById(canvasId);
    var div = document.getElementById(thediv_str);
    var ShowBarcodeCmd = true;
    var text = barcodeValue;
    var opts = "";
    var typeForCanvas = '2D';
    var opts = "readerinit=true";
    var ZValue = 3;
    var HorizontalScale = ZValue;
    var VerticalScale = ZValue;
    var barCodeType = "azteccode";  
    var bw = new ao;
    var tmp = opts.split(' ');
    opts = {};
    for (var i = 0; i < tmp.length; i++) {
        if (!tmp[i])
            continue;
        var eq = tmp[i].indexOf('=');
        if (eq == -1)
            opts[tmp[i]] = bw.value(true);
        else
            opts[tmp[i].substr(0, eq)] = bw.value(tmp[i].substr(eq + 1));
    }
    bw.bitmap(new rd);
    bw.scale(HorizontalScale, VerticalScale);
    bw.push(text);
    bw.push(opts);
    try {
        bw.call(barCodeType);
        bw.bitmap().show(canvasId, 'N', typeForCanvas, ZValue, ShowBarcodeCmd);
    } catch (e) {
    }
    return canvas.toDataURL();
}
function loadImage(rawImage)
{
    var div = document.getElementById("thediv");
    var image = new Image();
    image.src = "data:image/jpeg;base64," + rawImage
    p = document.createElement("p");
    p.align="center";
    p.appendChild(image);
    div.appendChild(p);
    resize_canvas();
}
function resize_canvas(){
    var div = document.getElementById("thediv");
    var canvas = document.getElementById("thecanvas");
    div.removeChild(canvas);
}
function getImageFromCanvas(canvasId) {
    var canvas = document.getElementById(canvasId);
    var img = document.createElement("img");
    img.src = canvas.toDataURL("image/png");
    return img;
}