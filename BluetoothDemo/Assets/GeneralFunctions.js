function isHexa(value)
{
	var i = 0;
		
	for (i = 0; i < value.length; i++)
	{
		if ((value[i] < "0") || (value[i] > "F") || ((value[i] > "9")  && (value[i] < "A")))
			return false;	
	}
	return true;
}



function HexToInt(value)
{
	var tmpVal = [];
	var i, j = 0;
	
	if (value.length % 2 != 0)
		return null;
		
		
	if (!isHexa(value))
		return null;

	for (i = 0; i < value.length; i += 2)
		tmpVal[j++] = parseInt(value.substr(i, 2), 16);  
	
	return tmpVal;
} 


function ISCPStrToHex(value)
{

	
	value = value.replace(/ /gi, "");
	

	value = value.replace(/<SW>/gi, "00");
	value = value.replace(/<CCMD>/gi, "01");
	value = value.replace(/<STR>/gi, "02");
	value = value.replace(/<SW.Base>/gi, "0300");
	value = value.replace(/<SW.Scanner>/gi, "0301");
	value = value.replace(/<CCMD.Base>/gi, "0400");
	value = value.replace(/<CCMD.Scanner>/gi, "0401");
	value = value.replace(/<STR.Base>/gi, "0500");
	value = value.replace(/<STR.Scanner>/gi, "0501");
	
	return (HexToInt(value));	
}

function ISCPStrToTGFArray(value)
{
	var tmptab = [];
	var tmpcmd = [];
	var length, type, fid, start, i = 0;
	var j = 0;
	
	value = ISCPStrToHex(value);
	if (value == null)
		return null;
	while (i < value.length)
	{
		start = i;
		
		if (i + 3> value.length)
			return (null);
			
		type = value[i++]; //type
		
		if (type > 0x02) //GIRQ
		{
			if (type > 0x05)
				return null;
			if (i + 3 > value.length)
				return (null);
			i++; // Saut Addressee
		}	
		
		i++; // saut gid
		fid = value[i++];
		
		switch (fid >> 6)
		{
			case 3:
			{
				if (i + 2 > value.length)
					return (null);
				length = value[i++] * 256 + value[i++];
			}  
			break;
			default: length = (fid >> 6); break;
		}
		
		if (i + length > value.length)
			return (null);
			
		i += length;
		tmpcmd = value.slice(start, i - start);
		tmptab[j++] = tmpcmd;
	}
	
	return (tmptab);
}


