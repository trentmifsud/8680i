function GenerateMenuCommandBarcode_AZTEC(menuCommandString)
{
   menuCommandString += "."; //Final Point for menu commands
   var img = CreatebwipBarcode(menuCommandString, "format=full readerinit=true", "azteccode");
   return img;
}

function GenerateMenuCommandBarcode_128(menuCommandString) 
{
   menuCommandString += "."; //Final Point for menu commands   
   var hex = a2hex(menuCommandString);
   var b96 = Get128ValuesMenuCommand(hex);
   var img = PrintCde128(b96);
   return img;
};

function a2hex(str)
{
  var arr = [];
  for (var i = 0, l = str.length; i < l; i ++) 
  {
      var hex = Number(str.charCodeAt(i)).toString(16);
      arr.push(hex);
  }
  return arr.join('');
}











