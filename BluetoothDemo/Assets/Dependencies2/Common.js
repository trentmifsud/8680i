function CreatebwipBarcode(barcodeString, barcodeOptions, symbology)
{
   var bw = new BWIPJS(Module, true);
   bw.bitmap(new Bitmap);
   bw.scale(2, 2);
   
   
   var tmp = barcodeOptions.split(' '); 
   barcodeOptions = {};
   for (var i = 0; i < tmp.length; i++) {
      if (!tmp[i])
         continue;
      var eq = tmp[i].indexOf('=');
      if (eq == -1)
         barcodeOptions[tmp[i]] = true;
      else
         barcodeOptions[tmp[i].substr(0, eq)] = tmp[i].substr(eq+1);
   }
   
   // Render the bar code
   try 
   {
      // Create a new BWIPP instance for each
      BWIPP()(bw, symbology, barcodeString, barcodeOptions);
   } 
   catch (e) 
   {
      // Watch for BWIPP generated raiseerror's.
      var msg = ''+e;
      if (msg.indexOf("bwipp.") >= 0) 
         alert(msg);
      else if (e.stack) 
         alert(e.stack);
      else 
         alert(e);
      return null;
   }
   
          
   //Create Image from Canvas
   var canvas = document.createElement("canvas"); 
   if(!canvas.getContext)
   {
      return;
   }
   
   var rot  = 'N'
   var context = canvas.getContext("2d");
   bw.bitmap().show(canvas, rot);
   
   var image= document.createElement("img");
   image.src = canvas.toDataURL("image/png");
   return image;
      
}


var Module = {
   // memoryInitializerPrefixURL:'your-path-here/',
   preRun:[ function() {
   } ],
   postRun:[ function() {
   } ]
};